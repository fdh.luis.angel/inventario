/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.fxventas;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import sistemainvetario.TabElements.LabTableElement;


import sistemainvetario.TabElements.ProductTableElement;
import sistemainvetario.conexiones.DbManager;
import sistemainvetario.tools.InvTools;
import sistemainvetario.tools.MyListManager;

/**
 *
 * @author LuisServer
 */
public class VentasController implements Initializable {
            
    @FXML private TableView<ProductTableElement> tabla_productos;
    @FXML private JFXTextField txt_producto;
    @FXML private TableView<ProductTableElement> tabla_venta;
    @FXML private Label lbl_total;
    @FXML private JFXTextField txt_dinero_cliente;
    @FXML private Label lbl_cambio_cliente;
    @FXML private Label lbl_producto;
    @FXML private JFXTextField txt_cantidad;
    @FXML private JFXTextField txt_usuario;
    @FXML private JFXPasswordField txt_contrasena;
    @FXML private AnchorPane panel_iniciar_sesion;
    @FXML private JFXTextField txt_db_name;
    @FXML private JFXPasswordField txt_db_password;
    @FXML private JFXTextField txt_ip_server;
    
    private DbManager dbManager = null;
    
    private MyListManager listManager;
    
    private String id_user = ""; 
    
    @FXML  private Label lbl_venta_anterior;
    @FXML  private TextArea info_area;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txt_ip_server.setText(InvTools.getServerIp());
        ProductTableElement.configProductTable(tabla_productos);
        listManager = new MyListManager(tabla_productos);
        //Conf tabla
        ProductTableElement.configProductTableUser(tabla_venta);
        
        //Solo numeros
        soloNumeros(txt_cantidad);
        soloNumeros(txt_dinero_cliente);
        
        //--------------- Desconectar de postgresql
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override public void run() {
                if (dbManager!=null)dbManager.desconectar();
            }
        });
    }    

    @FXML private void onKeyTyped(KeyEvent event) { 
        listManager.searchProduct(txt_producto.getText());
    }

    @FXML private void onEnterKey(ActionEvent event) {
        
        if (txt_producto.getText().length()<2) return;
        listManager.searchProduct(txt_producto.getText());
        if (tabla_productos.getItems()!=null && tabla_productos.getItems().size()>0) {
            ProductTableElement p = tabla_productos.getItems().get(0);
            
            boolean found = false;
            for (ProductTableElement prod : tabla_venta.getItems()) {
                if (p.getId().compareTo(prod.getId())==0) {
                    prod.sumarCantidad(getCantidad());
                    found = true;
                    break;
                }
            }
            
            if(!found){
                ProductTableElement p1 = new ProductTableElement(p.getId(), p.getNombre(), p.getCodigoBarras(), getCantidad(), p.getPrecio_unidad(), p.getPrecio_compra(), p.getLaboratorio());
                tabla_venta.getItems().add(p1);
            }
            tabla_venta.refresh();
        }       
        recalcularTotal();
        txt_producto.clear();
    }

    @FXML
    private void onBtnRegistrarVenta(ActionEvent event) {
        if (tabla_venta.getItems().size()==0) return;
        
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Area de ventas");
        
        ObservableList<LabTableElement> labList =  dbManager.getLaboratorios();
        
        boolean error = false;
        for (ProductTableElement p : tabla_venta.getItems()) {
            String idLab = "";
            
            for (LabTableElement l : labList) {
                if (l.getNombre().compareTo(p.getLaboratorio())==0) {
                    idLab = l.getId();
                    break;
                }
            }
            
            String query = String.format("select nueva_venta(%s,%s,'%s',%s,%s,%s);", p.getId(), p.getCantidad(), id_user, p.calcularSubTotal(),p.calcularGanancia(), idLab);   
            if (!dbManager.vender(query)) {
                alert.setHeaderText("Hubo un error");
                alert.setContentText(dbManager.getError());
                alert.showAndWait();
                error = true;
                info_area.appendText(InvTools.getPersonalLog("Error en venta"));
            }
        }
            
        if (!error) {
            alert.setHeaderText("El proceso finalizó correctamente");
            alert.setContentText("Se registró una venta por " + lbl_total.getText());
            lbl_venta_anterior.setText("Venta anterior por el valor de: $" + lbl_total.getText());
            info_area.appendText(InvTools.getPersonalLog("Se registró una venta por el valor de: $" + lbl_total.getText()));
            alert.showAndWait();
            tabla_venta.getItems().clear();
            lbl_total.setText("0.0");
            lbl_cambio_cliente.setText("Cambio: ");
            llenarListados();
        }        
    }
    
   
    @FXML  private void onBtnRemover(ActionEvent event) {
        ProductTableElement p = tabla_venta.getSelectionModel().getSelectedItem();
        if (p != null) {
            tabla_venta.getItems().remove(p);
            recalcularTotal();
        }
    }

    @FXML private void onBtnCancelar(ActionEvent event) {
        tabla_venta.getItems().clear();
        lbl_total.setText("0.0");
        lbl_cambio_cliente.setText("Cambio: ");
    }

    @FXML
    private void onBtnListo(ActionEvent event) {
        ProductTableElement p = (ProductTableElement)tabla_venta.getSelectionModel().getSelectedItem();
        if (p == null) return;
        p.setCantidad(getCantidad());
        tabla_venta.refresh();
        lbl_producto.setText("");
        txt_cantidad.setText("");
        
        recalcularTotal();
    }

    //Eventos del mouse
    @FXML private void mousePressedVentasTable(MouseEvent e) {
        if(e.getButton().equals(MouseButton.PRIMARY)){
            ProductTableElement p = (ProductTableElement)tabla_venta.getSelectionModel().getSelectedItem();
            if (p == null) return;
            if (e.getClickCount() == 2) {
                lbl_producto.setText(p.getNombre());
                txt_cantidad.setText(p.getCantidad());
                txt_cantidad.requestFocus();
            }
        }
    }

    @FXML private void onCantidadFinish(ActionEvent event) {
        onBtnListo(event);
    }

    @FXML private void mousePressedProductTable(MouseEvent e) {
        
        if(e.getButton().equals(MouseButton.PRIMARY)){
            ProductTableElement p = (ProductTableElement)tabla_productos.getSelectionModel().getSelectedItem();
            if (p == null) return;
            if (e.getClickCount() == 2) {
                /*
                ProductTableElement p1 = new ProductTableElement(p.getId(), p.getNombre(), p.getCodigoBarras(), getCantidad(), p.getPrecio_unidad(), p.getPrecio_compra(), p.getLaboratorio());
                tabla_venta.getItems().add(p1);
                recalcularTotal();*/
                
                boolean found = false;
                for (ProductTableElement prod : tabla_venta.getItems()) {
                    if (p.getId().compareTo(prod.getId())==0) {
                        prod.sumarCantidad(getCantidad());
                        found = true;
                        break;
                    }
                }
            
                if(!found){
                    ProductTableElement p1 = new ProductTableElement(p.getId(), p.getNombre(), p.getCodigoBarras(), getCantidad(), p.getPrecio_unidad(), p.getPrecio_compra(), p.getLaboratorio());
                    tabla_venta.getItems().add(p1);
                }
                tabla_venta.refresh();
            }            
        }
        recalcularTotal();
    }
    
    
    private String getCantidad(){
        if (txt_cantidad.getText().length()>0) {
            String cantidad = txt_cantidad.getText();
            txt_cantidad.setText("1");
            return cantidad;
        }
        return "1";
    }
    
    private int getTotal(){
        int total = 0;
        for (int i = 0; i < tabla_venta.getItems().size(); i++) {
            ProductTableElement p = (ProductTableElement)tabla_venta.getItems().get(i);
            int subtotal = Integer.parseInt(p.getSubTotal());
            total += subtotal;
        }
        return total;
    }
    
    private void recalcularTotal(){       
        lbl_total.setText(separarConComas(getTotal()));
        onDineroCliente(null);
    }

    @FXML
    private void onBtnIniciarSesion(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText("Ups, a Warning Dialog");
        
        dbManager = new DbManager(txt_ip_server.getText(), txt_db_name.getText(), "postgres", txt_db_password.getText());
        
        if (dbManager.estado) {
            if (dbManager.getLogginInfo(txt_usuario.getText(), "user", txt_contrasena.getText())
                || dbManager.getLogginInfo(txt_usuario.getText(), "admin", txt_contrasena.getText())) {
                panel_iniciar_sesion.setVisible(false);
                //actualizarInfo();
                llenarListados();
                id_user = txt_usuario.getText();
            }
            else{               
                alert.setContentText("Error al iniciar sesión");
                alert.showAndWait();
                dbManager.desconectar();
            }
        }
        else{
            alert.setHeaderText("Look, a Warning Dialog");
            alert.setContentText(dbManager.getError());
            alert.showAndWait();
        }        
    }
    
    private void soloNumeros(TextField txtField){
        txtField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("^([\\-|\\+]?[0-9]+)(\\.?[0-9]+)?$")) { 
                    txtField.setText(newValue.replaceAll("[^\\d-.]", ""));
                }
            }
        });
    }

    @FXML private void onDineroCliente(ActionEvent event) {
        if (txt_dinero_cliente.getText().length()>0 && lbl_total.getText().length()>0) {
            int total = getTotal();
            int cambio = Integer.parseInt(txt_dinero_cliente.getText());
            if ((cambio-total)>0) {
                lbl_cambio_cliente.setText("Cambio: "+separarConComas(cambio-total));
            }
            else{
                lbl_cambio_cliente.setText("Cambio: ");
            }            
        }        
    }
    
    private String separarConComas(int info){
        String numero = ""+info;
        String invertida = "";
        int d = 0;
        for (int i = numero.length()-1; i >=0 ; i--) {
            if (d<3) {
                invertida += numero.charAt(i);
                d++;
            }
            if (d==3 && i!=0) {
                invertida += ",";
                d=0;
            }
        }
        
        String retorno = "";
        for (int i = invertida.length()-1; i >=0 ; i--) {
            retorno += invertida.charAt(i);
        }        
        return retorno;
    }

    private void llenarListados() {
        listManager.setProductList(dbManager.getProductos());
    }

    @FXML
    private void onBtnActualizar(ActionEvent event) {
        llenarListados();
    }

    @FXML private void onSalesTableKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.DELETE) {
            onBtnRemover(null);
        }
    }
}
