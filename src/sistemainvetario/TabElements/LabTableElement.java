/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.TabElements;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author LuisServer
 */
public class LabTableElement {
    private SimpleStringProperty id;
    private SimpleStringProperty nombre;
    private SimpleStringProperty telefono;
    private SimpleStringProperty correo;
    private SimpleStringProperty direccion;
    private SimpleStringProperty descripcion;

    public LabTableElement(String id, String nombre, String telefono, String correo, String direccion, String descripcion) {
        this.id = new SimpleStringProperty(id);
        this.nombre = new SimpleStringProperty(nombre);
        this.telefono = new SimpleStringProperty(telefono);
        this.correo = new SimpleStringProperty(correo);
        this.direccion =  new SimpleStringProperty(direccion);
        this.descripcion =  new SimpleStringProperty(descripcion);
    }

    public String getId() {
        return id.get();
    }

    public String getNombre() {
        return nombre.get();
    }

    public String getTelefono() {
        return telefono.get();
    }

    public String getCorreo() {
        return correo.get();
    }

    public String getDireccion() {
        return direccion.get();
    }

    public String getDescripcion() {
        return descripcion.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public void setTelefono(String telefono) {
        this.telefono.set(telefono);
    }

    public void setCorreo(String correo) {
        this.correo.set(correo);
    }

    public void setDireccion(String direccion) {
        this.direccion.set(direccion);
    }

    public void setDescripcion(String descripcion) {
        this.descripcion.set(descripcion);
    }
    
    public void actualizar(String nom, String tel, String cor, String dir, String desc){
        setNombre(nom);
        setTelefono(tel);
        setCorreo(cor);
        setDireccion(dir);
        setDescripcion(desc);
    }
    
    public static void config(TableView userTable){
        TableColumn id = (TableColumn)userTable.getColumns().get(0);
        TableColumn nom = (TableColumn)userTable.getColumns().get(1);
        TableColumn tel = (TableColumn)userTable.getColumns().get(2);
        TableColumn cor = (TableColumn)userTable.getColumns().get(3);
        TableColumn dir = (TableColumn)userTable.getColumns().get(4);
        TableColumn desc = (TableColumn)userTable.getColumns().get(5);  
                
        id.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("id"));
        nom.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("nombre"));
        tel.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("telefono"));
        cor.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("correo"));
        dir.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("direccion"));
        desc.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("descripcion"));      
    }
    
    //--Nuevo constructor
    public LabTableElement(String ventas, String nombre) {
        this.id = new SimpleStringProperty(ventas);
        this.nombre = new SimpleStringProperty(nombre);
        this.telefono = new SimpleStringProperty();
        this.correo = new SimpleStringProperty();
        this.direccion =  new SimpleStringProperty();
        this.descripcion =  new SimpleStringProperty();
    }
    
    public static void configLabSales(TableView userTable){
        
        TableColumn vent = (TableColumn)userTable.getColumns().get(1);
        TableColumn nom = (TableColumn)userTable.getColumns().get(0);       
                
        vent.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("id"));
        nom.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("nombre"));
    }
}
