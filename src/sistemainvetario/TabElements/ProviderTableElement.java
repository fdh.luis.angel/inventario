/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.TabElements;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Claro
 */
public class ProviderTableElement {
    private SimpleStringProperty proveedor;
    private SimpleStringProperty telefono;
    private SimpleStringProperty direccion;
    private SimpleStringProperty correo;

    public ProviderTableElement(String proveedor, String telefono, String direccion, String correo) {
        this.proveedor = new SimpleStringProperty(proveedor);
        this.telefono = new SimpleStringProperty(telefono);
        this.direccion = new SimpleStringProperty(direccion);
        this.correo = new SimpleStringProperty(correo);
    }

    public String getProveedor() {
        return proveedor.get();
    }
    
    public void setProveedor(String proveedor) {
        this.proveedor.set(proveedor); 
    }

    public String getTelefono() {
        return telefono.get();
    }
    
    public void setTelefono(String telefono) {
        this.telefono.set(telefono); 
    }

    public String getDireccion() {
        return direccion.get();
    }

    public void setDireccion(String direccion) {
        this.direccion.set(direccion);
    }

    public String getCorreo() {
        return correo.get();
    }

    public void setCorreo(String correo) {
        this.correo.set(correo);
    }
    
    public static void configProviderTable(TableView providerTable) {
        TableColumn pro = (TableColumn)providerTable.getColumns().get(0);
        TableColumn tel = (TableColumn)providerTable.getColumns().get(1);
        TableColumn dir = (TableColumn)providerTable.getColumns().get(2);
        TableColumn cor = (TableColumn)providerTable.getColumns().get(3);
 
        
        pro.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("proveedor"));
        tel.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("telefono"));
        dir.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("direccion"));
        cor.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("correo"));        
        
        /*
        ObservableList listaProveedores =  FXCollections.observableArrayList();
        listaProveedores.add(new ProviderTableElement("OSA", "7202020", "Calle falsa#123", "@gmail.com"));
        
        providerTable.setItems(listaProveedores);
        */
    }
}
