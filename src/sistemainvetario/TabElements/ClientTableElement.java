/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.TabElements;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author LuisServer
 */
public class ClientTableElement {
    private SimpleStringProperty cliente;
    private SimpleStringProperty telefono;
    private SimpleStringProperty direccion;
    private SimpleStringProperty deuda;

    public ClientTableElement(String cliente, String telefono, String direccion, String deuda) {
        this.cliente = new SimpleStringProperty(cliente);
        this.telefono = new SimpleStringProperty(telefono);
        this.direccion = new SimpleStringProperty(direccion);
        this.deuda = new SimpleStringProperty(deuda);
    }

    public String getCliente() {
        return cliente.get();
    }

    public void setCliente(String cliente) {
        this.cliente.set(cliente);
    }

    public String getTelefono() {
        return telefono.get();
    }

    public void setTelefono(String telefono) {
        this.telefono.set(telefono);
    }

    public String getDireccion() {
        return direccion.get();
    }

    public void setDireccion(String direccion) {
        this.direccion.set(direccion);
    }

    public String getDeuda() {
        return deuda.get();
    }

    public void setDeuda(String deuda) {
        this.deuda.set(deuda);
    }
    
    
    public static void configClientTable(TableView userTable){
        TableColumn cli = (TableColumn)userTable.getColumns().get(0);
        TableColumn tel = (TableColumn)userTable.getColumns().get(1);
        TableColumn dir = (TableColumn)userTable.getColumns().get(2);
        TableColumn deu = (TableColumn)userTable.getColumns().get(3);
        
        cli.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("cliente"));
        tel.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("telefono"));
        dir.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("direccion"));
        deu.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("deuda"));
        /*
        ObservableList listaClientes =  FXCollections.observableArrayList();
        listaClientes.add(new ClientTableElement("Cliente1", "7202020", "Dir#123", "Debe 5000 de acetaminofen"));
        
        userTable.setItems(listaClientes);
        */
    }
}
