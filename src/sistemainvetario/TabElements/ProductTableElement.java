
package sistemainvetario.TabElements;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author LuisServer
 */
public class ProductTableElement {
    private SimpleStringProperty id;
    private SimpleStringProperty nombre;
    private SimpleStringProperty grupo;
    private SimpleStringProperty stock;
    private SimpleStringProperty precio_unidad;
    private SimpleStringProperty precio_compra;
    private SimpleStringProperty descripcion;
    private SimpleStringProperty fechaVencimiento;
    private SimpleStringProperty laboratorio;
    private SimpleStringProperty codigoBarras;
    private SimpleStringProperty ganancia;
    private SimpleStringProperty lote;
    
    private SimpleStringProperty cantidad;
    private SimpleStringProperty subTotal;
        
    
    public int precioVenta = 0;
    public int precioCompra = 0;

    public ProductTableElement(String id, String nombre, String codigoBarras, String cantidad, String precioVenta, String precioCompra, String laboratorio) {
        this.precioCompra = Integer.parseInt(precioCompra);
        this.precioVenta = Integer.parseInt(precioVenta);
        this.cantidad = new SimpleStringProperty();
        
        this.id = new SimpleStringProperty(id);
        this.nombre = new SimpleStringProperty(nombre);
        this.codigoBarras = new SimpleStringProperty(codigoBarras);
        this.subTotal = new SimpleStringProperty();
        this.laboratorio = new SimpleStringProperty(laboratorio); 
        
        setCantidad(cantidad);
    }

    //--- Usar solo desde  VENTAS
    public String calcularGanancia(){        
        int ganancia = (precioVenta-precioCompra)*Integer.parseInt(getCantidad());
        return ganancia + "";
    }
    
    public String calcularSubTotal(){        
        int total = precioVenta*Integer.parseInt(getCantidad());
        return total + "";
    }
   //--VENTAS

    public String getCantidad() {
        return cantidad.get();
    }

    public void setCantidad(String cantidad) {
        this.cantidad.set(cantidad);
        setSubTotal(calcularSubTotal());
    }
    
    public void sumarCantidad(String cantidad) {
        int cant = Integer.parseInt(this.getCantidad()) + Integer.parseInt(cantidad);        
        setCantidad(cant + "");
    }

    public String getSubTotal() {
        return subTotal.get();
    }

    public void setSubTotal(String subTotal) {
        this.subTotal.set(subTotal);
    }
    
    //Inicializarlos sin nulos psrs que funcione
    public ProductTableElement(String id, String nombre, String grupo, 
            String stock, String precio_unidad, String precio_compra, 
            String descripcion, String fechaVencimiento, String laboratorio,
            String codigoBarras, String ganan, String lote) {
        this.id = new SimpleStringProperty(id);
        this.nombre = new SimpleStringProperty(nombre);
        this.grupo = new SimpleStringProperty(grupo);
        this.stock = new SimpleStringProperty(stock);
        this.precio_unidad = new SimpleStringProperty(precio_unidad);
        this.precio_compra = new SimpleStringProperty(precio_compra);
        this.descripcion = new SimpleStringProperty(descripcion);
        this.fechaVencimiento = new SimpleStringProperty(fechaVencimiento);
        this.laboratorio =  new SimpleStringProperty(laboratorio);
        precioVenta = Integer.parseInt(precio_unidad);
        precioCompra = Integer.parseInt(precio_compra);
        this.codigoBarras = new SimpleStringProperty(codigoBarras);
        this.ganancia = new SimpleStringProperty(ganan);
        this.lote = new SimpleStringProperty(lote);
    }

    public String getId() {
        return id.get();
    }

    public String getNombre() {
        return nombre.get();
    }

    public String getGrupo() {
        return grupo.get();
    }

    public String getStock() {
        return stock.get();
    }

    public String getDescripcion() {
        return descripcion.get();
    }

    public String getFechaVencimiento() {
        return fechaVencimiento.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public void setGrupo(String grupo) {
        this.grupo.set(grupo);
    }

    public void setStock(String stock) {
        this.stock.set(stock);
    }

    public void setDescripcion(String descripcion) {
        this.descripcion.set(descripcion);
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento.set(fechaVencimiento);
    }

    public String getPrecio_unidad() {
        return precio_unidad.get();
    }

    public void setPrecio_unidad(String precio_unidad) {
        this.precio_unidad.set(precio_unidad);
    }

    public String getPrecio_compra() {
        return precio_compra.get();
    }

    public void setPrecio_compra(String precio_compra) {
        this.precio_compra.set(precio_compra);
    }

    public String getLaboratorio() {
        return laboratorio.get();
    }

    public void setLaboratorio(String laboratorio) {
        this.laboratorio.set(laboratorio);
    }

    public String getCodigoBarras() {
        return codigoBarras.get();
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras.set(codigoBarras);
    }

    public String getGanancia() {
        return ganancia.get();
    }

    public void setGanancia(String ganancia) {
        this.ganancia.set(ganancia);
    }

    public String getLote() {
        return lote.get();
    }

    public void setLote(String lote) {
        this.lote.set(lote);
    }
    
        
    public void actualizar(String nombre, String grupo, String stock, 
            String precio_unidad, String precio_compra, String descripcion, 
            String fechaVencimiento, String laboratorio, String codigoBarras,
            String ganan){
        setNombre(nombre);
        setGrupo(grupo);
        setStock(stock);
        setPrecio_unidad(precio_unidad);
        setPrecio_compra(precio_compra);
        setDescripcion(descripcion);
        setFechaVencimiento(fechaVencimiento);
        setLaboratorio(laboratorio);
        setCodigoBarras(codigoBarras);
        setGanancia(ganan);
    }
    
    public static void configProductTable(TableView userTable){
        TableColumn id = (TableColumn)userTable.getColumns().get(0);
        TableColumn nom = (TableColumn)userTable.getColumns().get(1);
        TableColumn gru = (TableColumn)userTable.getColumns().get(2);
        TableColumn sto = (TableColumn)userTable.getColumns().get(3);
        TableColumn pu = (TableColumn)userTable.getColumns().get(4);
        TableColumn pc = (TableColumn)userTable.getColumns().get(5);
        TableColumn desc = (TableColumn)userTable.getColumns().get(6);
        TableColumn fec = (TableColumn)userTable.getColumns().get(7);
        TableColumn lab = (TableColumn)userTable.getColumns().get(8);
        TableColumn cod = (TableColumn)userTable.getColumns().get(9);
        TableColumn pg = (TableColumn)userTable.getColumns().get(10);
        TableColumn lot = (TableColumn)userTable.getColumns().get(11);
                
        id.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("id"));
        nom.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("nombre"));
        gru.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("grupo"));
        sto.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("stock"));
        pu.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("precio_unidad"));
        pc.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("precio_compra"));
        desc.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("descripcion"));
        fec.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("fechaVencimiento"));
        lab.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("laboratorio"));
        cod.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("codigoBarras"));
        pg.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("ganancia"));
        lot.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("lote"));        
    }
    
    public static void configProductTableUser(TableView userTable){
        TableColumn id = (TableColumn)userTable.getColumns().get(0);
        TableColumn codbar = (TableColumn)userTable.getColumns().get(1);
        TableColumn pro = (TableColumn)userTable.getColumns().get(2);
        TableColumn can = (TableColumn)userTable.getColumns().get(3);
        TableColumn subt = (TableColumn)userTable.getColumns().get(4);
        TableColumn lab = (TableColumn)userTable.getColumns().get(5);
                
        id.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("id"));
        codbar.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("codigoBarras"));
        pro.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("nombre"));
        can.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("cantidad"));
        subt.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("subTotal"));
        lab.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("laboratorio"));
    }
}
