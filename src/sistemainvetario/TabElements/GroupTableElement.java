/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.TabElements;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author LuisServer
 */
public class GroupTableElement {
    private SimpleStringProperty id;
    private SimpleStringProperty nombre;
    private SimpleStringProperty descripcion;

    public GroupTableElement(String id, String nombre, String descripcion) {
        this.id = new SimpleStringProperty(id);
        this.nombre = new SimpleStringProperty(nombre);
        this.descripcion = new SimpleStringProperty(descripcion);
    }

    public String getId() {
        return id.get();
    }

    public String getNombre() {
        return nombre.get();
    }

    public String getDescripcion() {
        return descripcion.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public void setDescripcion(String descripcion) {
        this.descripcion.set(descripcion);
    }
    
    
    public static void configtTable(TableView userTable){
        TableColumn id = (TableColumn)userTable.getColumns().get(0);
        TableColumn nom = (TableColumn)userTable.getColumns().get(1);
        TableColumn desc = (TableColumn)userTable.getColumns().get(2);        
                
        id.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("id"));
        nom.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("nombre"));
        desc.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("descripcion"));
    }
    
    public void actualizar(String id, String nombre, String descripcion){
        setId(id);
        setNombre(nombre);
        setDescripcion(descripcion);
    }
}


