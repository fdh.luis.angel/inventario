/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.TabElements;

import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Luis Angel <fdh.luis.angel@gmail.com>
 */
public class UserTableElement {
    private SimpleStringProperty nombre;
    private SimpleStringProperty apellidos;
    private SimpleStringProperty cedula;
    private SimpleStringProperty telefono;
    private SimpleStringProperty correo;
    private SimpleStringProperty direccion;
    private SimpleStringProperty rol;
    private SimpleStringProperty password;

    public UserTableElement(String nombre, String apellidos, String cedula, String telefono, String correo, String direccion, String rol, String password) {
        this.nombre = new SimpleStringProperty(nombre);
        this.apellidos= new SimpleStringProperty(apellidos);
        this.cedula= new SimpleStringProperty(cedula);
        this.telefono= new SimpleStringProperty(telefono);
        this.correo= new SimpleStringProperty(correo);
        this.direccion= new SimpleStringProperty(direccion);
        this.rol= new SimpleStringProperty(rol);
        this.password= new SimpleStringProperty(password);
    }

    public String getNombre() {
        return nombre.get();
    }

    public String getApellidos() {
        return apellidos.get();
    }

    public String getCedula() {
        return cedula.get();
    }

    public String getTelefono() {
        return telefono.get();
    }

    
    public String getCorreo() {
        return correo.get();
    }

    public String getDireccion() {
        return direccion.get();
    }
    
    public String getRol() {
        return rol.get();
    }
    
    public String getPassword() {
        return password.get();
    }
    
    public void setNombre(String nombre) {
        this.nombre.set(nombre);
    }

    public void setApellidos(String apellidos) {
        this.apellidos.set(apellidos);
    }

    public void setCedula(String cedula) {
        this.cedula.set(cedula);
    }

    public void setTelefono(String telefono) {
        this.telefono.set(telefono);
    }

    public void setCorreo(String correo) {
        this.correo.set(correo);
    }

    public void setDireccion(String direccion) {
        this.direccion.set(direccion);
    }
    
    public void setRol(String rol) {
        this.rol.set(rol);
    }
    
    public void setPassword(String password) {
        this.password.set(password);
    }
    
    public void actualizar(String cedula,String nombre, String apellidos, String telefono, String correo, String direccion, String rol, String password) {
        setCedula(cedula);
        setNombre(nombre);
        setApellidos(apellidos);
        setTelefono(telefono);
        setCorreo(correo);
        setDireccion(direccion);
        setRol(rol);
        setPassword(password);
    }
    
    /**
     * This function configure user's table and adapt values of userTableElement 
     * (coorelation between class atributes and column's values)
     * @param userTable 
     */
    public static void configUserTable(TableView userTable){
        TableColumn nom = (TableColumn)userTable.getColumns().get(0);
        TableColumn ape = (TableColumn)userTable.getColumns().get(1);
        TableColumn ced = (TableColumn)userTable.getColumns().get(2);
        TableColumn tel = (TableColumn)userTable.getColumns().get(3);
        TableColumn cor = (TableColumn)userTable.getColumns().get(4);
        TableColumn dir = (TableColumn)userTable.getColumns().get(5);
        TableColumn rol = (TableColumn)userTable.getColumns().get(6);        
        
        nom.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("nombre"));
        ape.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("apellidos"));
        ced.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("cedula"));
        tel.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("telefono"));
        cor.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("correo"));
        dir.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("direccion"));
        rol.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("rol"));
    }
}
