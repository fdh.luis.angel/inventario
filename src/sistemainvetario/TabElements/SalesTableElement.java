/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.TabElements;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author LuisServer
 */
public class SalesTableElement {
    private SimpleStringProperty id;
    private SimpleStringProperty producto;
    private SimpleStringProperty unidades;
    private SimpleStringProperty usuario;
    private SimpleStringProperty fecha;
    private SimpleStringProperty valor;
    private SimpleStringProperty ganancia;
    private SimpleStringProperty laboratorio;

    public SalesTableElement(String id, String producto, String unidades, String usuario, String fecha, String valor, String ganancia, String laboratorio) {
        this.id = new SimpleStringProperty(id);
        this.producto = new SimpleStringProperty(producto);
        this.unidades = new SimpleStringProperty(unidades);
        this.usuario = new SimpleStringProperty(usuario);
        this.fecha = new SimpleStringProperty(fecha);
        this.valor = new SimpleStringProperty(valor);
        this.ganancia = new SimpleStringProperty(ganancia);
        this.laboratorio = new SimpleStringProperty(laboratorio);
    }

    public String getId() {
        return id.get();
    }

    public String getProducto() {
        return producto.get();
    }

    public String getUnidades() {
        return unidades.get();
    }

    public String getUsuario() {
        return usuario.get();
    }

    public String getFecha() {
        return fecha.get();
    }

    public String getValor() {
        return valor.get();
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public void setProducto(String producto) {
        this.producto.set(producto);
    }

    public void setUnidades(String unidades) {
        this.unidades.set(unidades);
    }

    public void setUsuario(String usuario) {
        this.usuario.set(usuario);
    }

    public void setFecha(String fecha) {
        this.fecha.set(fecha);
    }

    public void setValor(String valor) {
        this.valor.set(valor);
    }

    public String getGanancia() {
        return ganancia.get();
    }

    public void setGanancia(String ganancia) {
        this.ganancia.set(ganancia);
    }

    public String getLaboratorio() {
        return laboratorio.get();
    }

    public void setLaboratorio(String laboratorio) {
        this.laboratorio.set(laboratorio);
    }
    
    
    
    public static void configTable(TableView userTable){
        TableColumn id = (TableColumn)userTable.getColumns().get(0);
        TableColumn pro = (TableColumn)userTable.getColumns().get(1);
        TableColumn uni = (TableColumn)userTable.getColumns().get(2);
        TableColumn usu = (TableColumn)userTable.getColumns().get(3);
        TableColumn fe = (TableColumn)userTable.getColumns().get(4);
        TableColumn va = (TableColumn)userTable.getColumns().get(5);
        TableColumn ga = (TableColumn)userTable.getColumns().get(6);
        TableColumn la = (TableColumn)userTable.getColumns().get(7);
        
                
        id.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("id"));
        pro.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("producto"));
        uni.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("unidades"));
        usu.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("usuario"));
        fe.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("fecha"));
        va.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("valor"));
        ga.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("ganancia"));
        la.setCellValueFactory(new PropertyValueFactory<UserTableElement, String>("laboratorio"));             
        /*
        ObservableList listaventas =  FXCollections.observableArrayList();
        listaventas.add(new SalesTableElement("1", "Acetaminofen", "10", "Luis Angel", "2019/12/17", "5000"));
        
        userTable.setItems(listaventas);
    */
    }
}
