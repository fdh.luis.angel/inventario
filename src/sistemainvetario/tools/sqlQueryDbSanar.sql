--Configurar la hora en postgresSQL
set timezone TO 'UTC+5';


create table usuarios(
cedula varchar PRIMARY KEY,
nombres varchar,
apellidos varchar,
telefono varchar,
direccion varchar,
correo varchar,
rol varchar,
password varchar
);

create table grupos(
id_grupo serial PRIMARY KEY,
nombre varchar,
descripcion varchar
);

create table productos(
id_producto serial PRIMARY KEY,
nombre varchar,
id_grupo INT,
stock INT,
precio_unidad INT,
precio_compra INT,
descripcion varchar,
fecha_vencimiento date,
FOREIGN KEY (id_grupo) REFERENCES grupos (id_grupo)
);

create table ventas(
id_venta serial PRIMARY KEY,
id_producto INT,
unidades INT,
cedula varchar,
fecha timestamp,
valor INT,
FOREIGN KEY (id_producto) references productos(id_producto),
FOREIGN KEY (cedula) references usuarios(cedula)
);
--prueba de insertar datos

insert into usuarios(cedula, nombres, apellidos, telefono, direccion, correo, rol) values
	('1085320429', 'Luis Angel', 'Espana Yepez', '3002384972', 'CRA 4 # 123 Santa Monica', 'correo@gmail.com', 'user');

insert into grupos(nombre, descripcion) values('grupo1', 'descripcion del grupo');

insert into productos(nombre, id_grupo, stock, precio_unidad, precio_compra, descripcion, fecha_vencimiento) values
('Dolex', 2 , 4, 1000, 850, 'Para dolores simples', '2020/12/18');

insert into ventas(id_producto, unidades, cedula, fecha, valor) values
(2,10,'1085320429',(select localtimestamp), 1000);


DROP FUNCTION nueva_venta(id_producto_f INT, unidades_f INT, cedula_f varchar, valor_f INT);
create or replace function nueva_venta(id_producto_f INT, unidades_f INT, cedula_f varchar, valor_f INT) returns void as $$
	BEGIN 
		UPDATE productos set stock=(productos.stock-unidades_f) where productos.id_producto = id_producto_f;

		insert into ventas(id_producto, unidades, cedula, fecha, valor) values
		(id_producto_f,unidades_f,cedula_f,(select localtimestamp),valor_f);
	END;
	$$ LANGUAGE plpgsql;

--Nueva venta
select nueva_venta(2,1,'1085320429',1000);

--Visualizar tabla ventas
SELECT id_venta,productos.nombre, ventas.unidades,  usuarios.nombres,ventas.fecha, ventas.valor
FROM ventas join productos on ventas.id_producto = productos.id_producto
join usuarios on ventas.cedula=usuarios.cedula order by ventas.fecha;

--Visualizar los productos
select id_producto, productos.nombre as nom_producto, grupos.nombre as nom_producto, 
stock, precio_unidad, precio_compra, productos.descripcion, fecha_vencimiento from
productos join grupos on productos.id_grupo = grupos.id_grupo;

--Visualizar todos los usuarios
select nombres, apellidos, cedula, telefono, correo, direccion, rol, usuarios.password as contrasena from usuarios;

--ver los grupos
select * from grupos;

/*Funcion que podria servir luego...
DROP FUNCTION agregarProducto(id_producto INT, nombre varchar, nom_grupo VARCHAR, stock INT, precio_unidad INT, precio_compra INT, descripcion varchar, fecha_venciminento date);
create or replace function agregarProducto(id_producto INT, nombre varchar, nom_grupo VARCHAR, stock INT, precio_unidad INT, precio_compra INT, descripcion varchar, fecha_venciminento date) returns text as $$
	DECLARE
	resultado TEXT DEFAULT 'No es nulo';
	BEGIN 
		IF id_producto IS null then 
		resultado := 'Es nula esa mondA';
		END IF;
		RETURN resultado;
	END;
	$$ LANGUAGE plpgsql;
	
select agregarProducto(4, 'nom', 'nom_grup', 1,2,3, 'desc', '2019/12/17');
*/

ALTER TABLE usuarios ADD COLUMN password varchar;

--Cuantos productos hay
select count("id_producto")as total_productos from productos;

--Cuantas ventas hay en un dia especifico
select count("id_venta") as total_ventas from ventas where fecha > '2019-12-18';

--Cuantos usuarios hay registrados
select count("cedula") from usuarios;

-- ventas en un periodo especifico
SELECT id_venta,productos.nombre, ventas.unidades,  usuarios.nombres as usuario,ventas.fecha, ventas.valor
FROM ventas join productos on ventas.id_producto = productos.id_producto
join usuarios on ventas.cedula=usuarios.cedula where fecha >= '2019-12-16' and fecha <= '2019-12-18' order by fecha;


--Eliminar usuarios
delete from usuarios where cedula like '1085320429';

--Eliminar productos
delete from productos where id_producto = 6;

--Eliminar grupo
delete from grupos where id_grupo = 2;

--Ver datos para loggin
select cedula, rol, usuarios.password as contrasena from usuarios;

--Editar usuario
DROP FUNCTION editarUsuario(ced varchar, nom VARCHAR, ape varchar, tel varchar, dir varchar, correo varchar, rol_usuario varchar, pass varchar);
create or replace function editarUsuario(ced varchar, nom VARCHAR, ape varchar, tel varchar, dir varchar, cor varchar, rol_usuario varchar, pass varchar) returns void as $$
BEGIN 
	UPDATE usuarios set nombres=nom where cedula = ced;
	UPDATE usuarios set apellidos=ape where cedula = ced;
	UPDATE usuarios set telefono=tel where cedula = ced;
	UPDATE usuarios set direccion=dir where cedula = ced;
	UPDATE usuarios set correo=cor where cedula = ced;
	UPDATE usuarios set rol=rol_usuario where cedula = ced;
	UPDATE usuarios set password=pass where cedula = ced;
END;
$$ LANGUAGE plpgsql;
select editarusuario('1085320429', 'Luisito', 'Comunica', '318318318', 'calle falsa 123', '@mail.123', 'admin', '4321');


--Editar producto

DROP FUNCTION editarProducto(id_prod INT, nom VARCHAR, id_grup INT, sto INT, precio_u INT, precio_c INT, descrip varchar, fech_ven date);
create or replace function editarProducto(id_prod INT, nom VARCHAR, id_grup INT, sto INT, precio_u INT, precio_c INT, descrip varchar, fech_ven date) 
returns void as $$
BEGIN 
	UPDATE productos set nombre=nom where id_producto = id_prod;
	UPDATE productos set id_grupo=id_grup where id_producto = id_prod;
	UPDATE productos set stock=sto where id_producto = id_prod;
	UPDATE productos set precio_unidad=precio_u where id_producto = id_prod;
	UPDATE productos set precio_compra=precio_c where id_producto = id_prod;
	UPDATE productos set descripcion=descrip where id_producto = id_prod;
	UPDATE productos set fecha_vencimiento=fech_ven where id_producto = id_prod;
END;
$$ LANGUAGE plpgsql;

select editarProducto(6, 'Dolex', 2, 20, 1100, 900, 'Para dolores tome doloran', '2020-1-1');


--Editar grupo

DROP FUNCTION editarGrupo(id_grup INT, nom VARCHAR, descrip varchar);
create or replace function editarGrupo(id_grup INT, nom VARCHAR, descrip varchar) 
returns void as $$
BEGIN 
	UPDATE grupos set nombre=nom where id_grup = id_grupo;
	UPDATE grupos set descripcion=descrip where id_grup = id_grupo;
END;
$$ LANGUAGE plpgsql;

select editarGrupo(2,'OSA', 'Osa es una empresa :v');

--Eliminar un grupo
delete from grupos where id_grupo = 6;


SELECT id_venta,productos.nombre, ventas.unidades,  usuarios.nombres,ventas.fecha, ventas.valor 
FROM ventas join productos on ventas.id_producto = productos.id_producto 
join usuarios on ventas.cedula=usuarios.cedula order by ventas.fecha;

--conteo de ventas
select nombres, sum(unidades) as num_ventas, date(fecha) as mi_fecha
from ventas natural join usuarios group by 1, 3 order by mi_fecha;

--Conteo ventas en rango
select nombres, sum(unidades) as num_ventas, date(fecha) as mi_fecha 
from ventas natural join usuarios 
where fecha >= '2019-12-17' and fecha <= '2019-12-30'
group by 1, 3 order by mi_fecha;