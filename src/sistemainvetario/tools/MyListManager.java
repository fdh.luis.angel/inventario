/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.tools;

import java.util.function.Predicate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.scene.chart.PieChart;
import javafx.scene.control.TableView;
import sistemainvetario.TabElements.ClientTableElement;
import sistemainvetario.TabElements.GroupTableElement;
import sistemainvetario.TabElements.LabTableElement;
import sistemainvetario.TabElements.ProductTableElement;
import sistemainvetario.TabElements.ProviderTableElement;
import sistemainvetario.TabElements.SalesTableElement;
import sistemainvetario.TabElements.UserTableElement;

/**
 *
 * @author LuisServer
 */
public class MyListManager {
    private ObservableList<UserTableElement> userList;
    private ObservableList<ProviderTableElement> providerList;
    private ObservableList<ClientTableElement> clientList;
    private ObservableList<ProductTableElement> productList;
    private ObservableList<GroupTableElement> groupList;
    private ObservableList<SalesTableElement> salesList;
    private ObservableList<LabTableElement> labList;
    private ObservableList<LabTableElement> labSalesList;
    
    private TableView<UserTableElement> tabla_usuarios;
    private TableView<ProviderTableElement> tabla_proveedores;
    private TableView<ClientTableElement> tabla_cliente;
    private TableView<ProductTableElement> tabla_productos;
    private TableView<GroupTableElement> tabla_grupos;
    private TableView<SalesTableElement> tabla_ventas;
    private TableView<LabTableElement> tabla_laboratorios;
    private TableView<LabTableElement> tabla_ventas_laboratorios;

    private FilteredList<UserTableElement> filteredUser;
    private FilteredList<ProviderTableElement> filteredProvider;
    private FilteredList<ClientTableElement> filteredClient;
    private FilteredList<ProductTableElement> filteredProduct;
    private FilteredList<GroupTableElement> filteredGroup;
    private FilteredList<LabTableElement> filteredLab;

    public MyListManager( TableView tabla_productos) {
        this.tabla_productos = tabla_productos;
        
        productList =  FXCollections.observableArrayList();
        
        filteredProduct = new FilteredList<>(productList);
    }
    
    
    
    public MyListManager(TableView tabla_usuarios, TableView tabla_proveedores, TableView tabla_cliente, TableView tabla_productos, TableView tabla_grupos, TableView tabla_ventas, TableView tabla_laboratorios, TableView tabla_ventas_lab) {
        this.tabla_usuarios = tabla_usuarios;
        this.tabla_proveedores = tabla_proveedores;
        this.tabla_cliente = tabla_cliente;
        this.tabla_productos = tabla_productos;
        this.tabla_grupos = tabla_grupos;
        this.tabla_ventas = tabla_ventas;
        this.tabla_laboratorios = tabla_laboratorios;
        this.tabla_ventas_laboratorios = tabla_ventas_lab;
        
        
        userList =  FXCollections.observableArrayList();
        providerList =  FXCollections.observableArrayList();
        clientList =  FXCollections.observableArrayList();
        productList =  FXCollections.observableArrayList();
        groupList =  FXCollections.observableArrayList();
        salesList =  FXCollections.observableArrayList();
        labList = FXCollections.observableArrayList();
        labSalesList = FXCollections.observableArrayList();
        
        this.tabla_usuarios.setItems(userList);
        this.tabla_proveedores.setItems(providerList);
        this.tabla_cliente.setItems(clientList);
        this.tabla_productos.setItems(productList);
        this.tabla_grupos.setItems(groupList);   
        this.tabla_ventas.setItems(salesList);
        this.tabla_laboratorios.setItems(labList);
        this.tabla_ventas_laboratorios.setItems(labSalesList);
        
        filteredUser = new FilteredList<>(userList);
        filteredProvider = new FilteredList<>(providerList);
        filteredClient = new FilteredList<>(clientList);
        filteredProduct = new FilteredList<>(productList);
        filteredGroup = new FilteredList<>(groupList);
        filteredLab = new FilteredList<>(labList);
    }
    

    public ObservableList getUserList() {
        return userList;
    }

    public void setUserList(ObservableList userList) {
        this.userList.clear();
        this.userList.addAll(userList);
    }

    public ObservableList getProviderList() {
        return providerList;
    }

    public void setProviderList(ObservableList providerList) {
        this.providerList = providerList;
    }

    public ObservableList getClientList() {
        return clientList;
    }

    public void setClientList(ObservableList clientList) {
        this.clientList = clientList;
    }

    public ObservableList getProductList() {
        return productList;
    }

    public void setProductList(ObservableList productList) {
        this.productList.clear();
        this.productList.addAll(productList);
        tabla_productos.refresh();
    }

    public ObservableList getGroupList() {
        return groupList;
    }

    public void setGroupList(ObservableList groupList) {
        this.groupList.clear();
        this.groupList.addAll(groupList);
    }
    
    public GroupTableElement getGroupFromName(String name){
        for (int i = 0; i < groupList.size(); i++) {
            if (name.compareTo(groupList.get(i).getNombre())==0) {
                return groupList.get(i);
            }
        }
        return null;
    }
    
    public LabTableElement getLabFromName(String name){
        for (int i = 0; i < labList.size(); i++) {
            if (name.compareTo(labList.get(i).getNombre())==0) {
                return labList.get(i);
            }
        }
        return null;
    }
    //----------------------------------------------------------------- USERS
    public void searchUser(String user){
        filteredUser.setPredicate(new Predicate<UserTableElement>(){
            @Override public boolean test(UserTableElement usuario) {
                if (user==null)return true;
                if (user.length()==0)return true;
                
                String lowerCase = user.toLowerCase();
                
                if (usuario.getNombre().toLowerCase().indexOf(lowerCase)!= -1) {return true;}
                return false;
            }
        });
        
        SortedList<UserTableElement> sortUser = new SortedList<>(filteredUser);        
        sortUser.comparatorProperty().bind(tabla_usuarios.comparatorProperty());        
        tabla_usuarios.setItems(sortUser);
    }
    
        
    //----------------------------------------------------------------- PRODUCTOS
    public void newProduct(ProductTableElement product){
        productList.add(product);
        tabla_productos.refresh();
    }
    
    public void searchProduct(String product){
        filteredProduct.setPredicate(new Predicate<ProductTableElement>(){
            @Override public boolean test(ProductTableElement producto) {
                if (product==null)return true;
                if (product.length()==0)return true;
                
                String lowerCase = product.toLowerCase();
                
                if (producto.getNombre().toLowerCase().indexOf(lowerCase)!= -1 ||
                    producto.getLaboratorio().toLowerCase().indexOf(lowerCase)!= -1||
                    producto.getCodigoBarras().toLowerCase().indexOf(lowerCase)!= -1) 
                {return true;}
                return false;
            }
        });
        
        SortedList<ProductTableElement> sortProd = new SortedList<>(filteredProduct);        
        sortProd.comparatorProperty().bind(tabla_productos.comparatorProperty());        
        tabla_productos.setItems(sortProd);
    }

    public ObservableList<SalesTableElement> getSalesList() {
        return salesList;
    }

    public void setSalesList(ObservableList<SalesTableElement> salesList) {
        this.salesList.clear();
        this.salesList.addAll(salesList);
    }

    public ObservableList<LabTableElement> getLabList() {
        return labList;
    }

    public void setLabList(ObservableList<LabTableElement> labList) {
        this.labList.clear();
        this.labList.addAll(labList);
    }

    public void setSalesLabList(ObservableList<PieChart.Data> chartList) {
        
        ObservableList<LabTableElement> local = FXCollections.observableArrayList();
        
        for (PieChart.Data d : chartList) {
            local.add(new LabTableElement((int)(d.getPieValue())+"", d.getName()));
        }
        
        this.labSalesList.clear();
        this.labSalesList.addAll(local);
    }    
    
    
    public void searchLaboratory(String laboratory){
        filteredLab.setPredicate(new Predicate<LabTableElement>(){
            @Override public boolean test(LabTableElement lab) {
                if (laboratory==null)return true;
                if (laboratory.length()==0)return true;
                
                String lowerCase = laboratory.toLowerCase();
                
                if (lab.getNombre().toLowerCase().indexOf(lowerCase)!= -1) 
                {return true;}
                return false;
            }
        });
        
        SortedList<LabTableElement> sortLab = new SortedList<>(filteredLab);        
        sortLab.comparatorProperty().bind(tabla_laboratorios.comparatorProperty());        
        tabla_laboratorios.setItems(sortLab);
    }
}
