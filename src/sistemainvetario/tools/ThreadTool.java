/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.tools;

import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import sistemainvetario.inventario.InventarioController;

/**
 *
 * @author LuisServer
 */
public class ThreadTool extends Thread{
    private InventarioController interfaz;
    private static int ACTUALIZACION = 30000;
    private static int FECHA_VENCIMIENTO = 7200000; 

    public ThreadTool(InventarioController interfaz) {
        this.interfaz = interfaz;
        setDaemon(true);
    }
    
    @Override
    public void run() {
        int counter = FECHA_VENCIMIENTO;
        while(true){            
            try {    
                sleep(ACTUALIZACION);
                Platform.runLater(new Runnable() {
                    @Override  public void run() {
                        interfaz.actualizarInfo();
                    }
                });
                counter += ACTUALIZACION;
                if (counter >= FECHA_VENCIMIENTO) {
                    counter = 0;
                    Platform.runLater(new Runnable() {
                        @Override  public void run() {
                            interfaz.validarVencimiento();
                            interfaz.validarStock();
                        }
                    });
                }                
            } catch (InterruptedException ex) {
                Logger.getLogger(ThreadTool.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
