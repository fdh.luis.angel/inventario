/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import sistemainvetario.TabElements.SalesTableElement;
import sistemainvetario.conexiones.DbManager;

/**
 *
 * @author LuisServer
 */
public class InvTools {
    
    public static void setChartInfo(AreaChart chart_ventas, LinkedList<String> result){
        //XYChart.Series series = new XYChart.Series();
        LinkedList<XYChart.Series> series = new LinkedList<>();
        
        for (int i = 0; i < result.size(); i++) {
            boolean found = false;
            for (int j = 0; j < series.size(); j++) {
                if (result.get(i).split(";")[0].compareTo(series.get(j).getName())==0) {
                    found = true;
                    int ventas = Integer.parseInt(result.get(i).split(";")[1]);
                    series.get(j).getData().add(new XYChart.Data(result.get(i).split(";")[2], ventas));
                    break;
                }
            }
            if (!found) {
                XYChart.Series s = new XYChart.Series();
                int ventas = Integer.parseInt(result.get(i).split(";")[1]);
                s.getData().add(new XYChart.Data(result.get(i).split(";")[2], ventas));
                s.setName(result.get(i).split(";")[0]);
                series.add(s);
            }
        }
        
        chart_ventas.getData().clear();
        chart_ventas.getData().addAll(series);
    }
    
    public static void setChartInfo(PieChart chart_laboratorios, ObservableList<PieChart.Data> info){
        chart_laboratorios.getData().clear();
        chart_laboratorios.setData(info);
    }
    
    public static int getValorVentas(ObservableList<SalesTableElement> salesList){
        int total = 0;
        for (SalesTableElement s : salesList) {
            total += Integer.parseInt(s.getValor());
        }
        return total;
    }
    
    public static int getGananciasVentas(ObservableList<SalesTableElement> salesList){
        int total = 0;
        for (SalesTableElement s : salesList) {
            total += Integer.parseInt(s.getGanancia());
        }
        return total;
    }
    
    public static String getPersonalLog(String user_info){
        java.util.Date date=new java.util.Date(System.currentTimeMillis());  
        return date.toString() + "   " + user_info+"\n";
    }
    
    public static long getDaysInRange(String date1){
        LocalDate d1 = LocalDate.parse(java.time.LocalDate.now().toString(), DateTimeFormatter.ISO_LOCAL_DATE);
        LocalDate d2 = LocalDate.parse(date1, DateTimeFormatter.ISO_LOCAL_DATE);
        Duration diff = Duration.between(d1.atStartOfDay(), d2.atStartOfDay());
        long diffDays = diff.toDays();
        return diff.toDays();
    }
    
    public static String calcularPrecio(String price, String porcent){
        if (price.length() ==0 || porcent.length() == 0)return price+"";
        double por = Integer.parseInt(porcent)/100.0;
        int pri = Integer.parseInt(price);
        
        double res = pri*(1+por);

        return redondeo((int)res);
    }  

        
    public static String redondeo(int numero){
        String num = numero + "";
        String subS = num.substring(num.length()-2,num.length());

        if(Integer.parseInt(subS)< 30){
            return num.substring(0,num.length()-2) + "00";
        }
        else if(Integer.parseInt(subS)< 70){
            return num.substring(0,num.length()-2) + "50";
        }
        else{
            int local_num = numero;
            while(local_num%50 != 0){
                local_num ++;
            }
            return local_num+"";
        } 
    }
    
    public static String getServerIp(){
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        String res = "";
        try {
            archivo = new File ("server.txt");
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);
            String linea;
            while((linea=br.readLine())!=null)
                res = linea;
        }
        catch(Exception e){
            e.printStackTrace();
        }finally{
            try{                    
                if( null != fr )fr.close();                
            }catch (Exception e2){ 
                e2.printStackTrace();
            }
        }
        if (res.length()==0)res = "localhost";
        
        return res;
    }
    
    public static void saveServerIp(String serverIp){
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter("server.txt");
            pw = new PrintWriter(fichero);
            pw.println(serverIp);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
                if (null != fichero)fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
    }
    
    public static void main(String[] args) {
        //System.out.println(java.time.LocalDateTime.now().toString());
        //System.out.println(java.time.LocalDate.now().toString());
        //System.out.println(getPersonalLog("Test"));
        //System.out.println("Diff "+ getDaysInRange(""));
        System.out.println(calcularPrecio("700", "5"));
    }
}

