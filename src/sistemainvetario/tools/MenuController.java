/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.tools;


import com.jfoenix.controls.JFXTabPane;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 *
 * @author Luis España
 */
public class MenuController {
    public static void onCardClick(MouseEvent e, Pane indicador, JFXTabPane tabPane) {
        Node card = (Node)e.getSource();
        if (card.getId()== null) {
            AnchorPane parent = (AnchorPane)card.getParent();
            indicador.setLayoutY(card.getLayoutY()+parent.getLayoutY());
            indicador.setId("color_verde");
        }
        else{            
            indicador.setLayoutY(card.getLayoutY());
            indicador.setId("color_azul_claro");
            
        }
        tabPane.getSelectionModel().select(Integer.parseInt(card.getAccessibleText()));
    }    
    
}
