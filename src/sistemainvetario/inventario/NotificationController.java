/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.inventario;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Claro
 */
public class NotificationController implements Initializable{

    @FXML public AnchorPane root;
    @FXML public Label lbl_titulo;
    @FXML public Label lbl_informacion;
    @FXML public ImageView img_icon;
    @FXML public Pane btn_cerrar;
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
    }
    
    @FXML private void onBtnCerrar(MouseEvent e){
        AnchorPane pane = (AnchorPane)e.getSource();
        pane = (AnchorPane)pane.getParent();
        VBox parent = (VBox)pane.getParent();
        parent.getChildren().remove(pane);
        System.out.println("Cerrar "+this.toString());
    }
    
    
}
