package sistemainvetario.inventario;


import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.DirectoryChooser;
import javafx.util.StringConverter;


import sistemainvetario.TabElements.ClientTableElement;
import sistemainvetario.TabElements.GroupTableElement;
import sistemainvetario.TabElements.LabTableElement;
import sistemainvetario.TabElements.ProductTableElement;
import sistemainvetario.TabElements.ProviderTableElement;
import sistemainvetario.TabElements.SalesTableElement;
import sistemainvetario.TabElements.UserTableElement;
import sistemainvetario.conexiones.DbManager;
import sistemainvetario.tools.InvTools;
import sistemainvetario.tools.MenuController;
import sistemainvetario.tools.MyListManager;
import sistemainvetario.tools.ThreadTool;


/**
 *
 * @author Luis España
 */
public class InventarioController implements Initializable {
    //-------------- 
    private URL url;
    private MyListManager listManager;
    private DbManager dbManager;
    
    //Panel inicial 
    @FXML private JFXTextField txt_usuario;
    @FXML private JFXPasswordField txt_contrasena;
    
    @FXML private JFXTextField txt_servidor;
    @FXML private JFXTextField txt_nombre_base;
    @FXML private JFXPasswordField txt_clave_base;
    @FXML private AnchorPane panel_iniciar_sesion;
    //---------------------------------------------
    
    @FXML private Pane indicador;
    @FXML private AnchorPane menu_lateral;
    @FXML private VBox notification_area;
    @FXML private Button add_notification;
    @FXML private Label lbl_notificaciones;
    @FXML private JFXTabPane tabPane;
    
    // Componentes del menù
    @FXML private Label lbl_usuario;
    @FXML private Label lbl_estado_conexion;
    @FXML private Circle led_estado;
    
    // Componentes de tabPane Home (inicio)
    @FXML private Label lbl_productos_registrados;
    @FXML private Label lbl_ventas_nuevas;
    @FXML private Label lbl_total_usuarios;
    @FXML private Label lbl_porcentaje;
    @FXML private TextArea info_area;
    @FXML private Label lbl_usuarios;
    
    //Componentes de usuarios
    @FXML private JFXTextField txt_u_nombres;
    @FXML private JFXTextField txt_u_apellidos;
    @FXML private JFXTextField txt_u_cedula;
    @FXML private JFXTextField txt_u_telefono;
    @FXML private JFXTextField txt_u_direccion;
    @FXML private JFXTextField txt_u_correo;        
    @FXML private JFXPasswordField txt_u_password;
    @FXML private JFXComboBox<String> txt_u_rol;
   
    @FXML private JFXTextField txt_u_search;
    @FXML private TableView<UserTableElement> tabla_usuarios;
    
    //Componentes TAB clientes    
    @FXML private TableView tabla_clientes;
    
    //Componentes TAB grupos
    @FXML private JFXTextField txt_grup_id;
    @FXML private JFXTextField txt_grup_nom;
    @FXML private JFXTextField txt_grup_desc;    
    @FXML private TableView<GroupTableElement> tabla_grupos;
    
    //Componentes TAB ventas
    @FXML private TableView<SalesTableElement> tabla_ventas;
    @FXML private AreaChart<?, ?> chart_ventas;    
    @FXML private JFXDatePicker date_ventas_inicio;
    @FXML private JFXDatePicker date_ventas_final;
    @FXML private Label lbl_sales_total_ventas;
    @FXML private Label lbl_sales_valor_ventas;
    
    
    //Componentes TAB proveedores
    @FXML private JFXTextField txt_prov_proveedor;
    @FXML private JFXTextField txt_prov_telefono;
    @FXML private JFXTextField txt_prov_direccion;
    @FXML private JFXTextField txt_prov_correo;    
    @FXML private TableView tabla_proveedores;
    
    
    //Componentes TAB productos
    @FXML private JFXTextField txt_prod_id;
    @FXML private JFXTextField txt_prod_nom;
    @FXML private JFXComboBox<GroupTableElement> cbx_prod_grup;
    @FXML private JFXTextField txt_prod_stock;
    @FXML private JFXTextField txt_prod_pu;
    @FXML private JFXTextField txt_prod_pc;
    @FXML private JFXTextField txt_prod_desc;
    @FXML private JFXDatePicker date_prod_vence;
    @FXML private JFXTextField txt_prod_porc;
    @FXML private JFXTextField txt_prod_codbar;
    @FXML private JFXComboBox<LabTableElement> cbx_prod_lab;    
    @FXML private TableView<ProductTableElement> tabla_productos;
    @FXML private JFXTextField txt_prod_search;
    @FXML private JFXTextField txt_prod_lote;
    
    
    //--Laboratorios
    @FXML private JFXTextField txt_lab_id;
    @FXML private JFXTextField txt_lab_direccion;
    @FXML private JFXTextField txt_lab_telefono;
    @FXML private TableView<LabTableElement> tabla_laboratorios;
    @FXML private JFXTextField txt_lab_nombre;
    @FXML private JFXTextField txt_lab_descripcion;
    @FXML private JFXTextField txt_lab_correo;
    @FXML private JFXTextField txt_lab_search;
    
    //-- Clasificar
    @FXML
    private JFXTextField txt_prod_sumarUni;
    @FXML
    private Label lbl_sales_ganancias;
    @FXML
    private PieChart chart_laboratorios;
    @FXML
    private TableView<LabTableElement> tabla_ventas_lab;
   
    
    
    
    
         
    @Override public void initialize(URL url, ResourceBundle rb) {
        this.url = url;
        
        //Verificar si hay archivo y ponerle el ultimo servidor
        txt_servidor.setText(InvTools.getServerIp());
        
        listManager = new MyListManager(tabla_usuarios, tabla_proveedores, tabla_clientes, tabla_productos, tabla_grupos, tabla_ventas, tabla_laboratorios,tabla_ventas_lab);
        
        notification_area.heightProperty().addListener(new ChangeListener<Number>() {
            @Override 
            public void changed(ObservableValue<? extends Number> observableValue, Number oldSceneHeight, Number newSceneHeight) {
                if (newSceneHeight.doubleValue()>0) {
                    lbl_notificaciones.setVisible(true);
                    lbl_notificaciones.setText(notification_area.getChildren().size()+"");
                }
                else{
                    lbl_notificaciones.setVisible(false);
                }
            }
        });
        //Inicializar las tablas y agregar elementos a la tabla
        UserTableElement.configUserTable(tabla_usuarios);
        ProviderTableElement.configProviderTable(tabla_proveedores);
        ClientTableElement.configClientTable(tabla_clientes);
        ProductTableElement.configProductTable(tabla_productos);
        GroupTableElement.configtTable(tabla_grupos);
        SalesTableElement.configTable(tabla_ventas);
        LabTableElement.config(tabla_laboratorios);
        LabTableElement.configLabSales(tabla_ventas_lab);
        
        //Inicializar comboBox
        cbx_prod_grup.setConverter(new StringConverter<GroupTableElement>(){
            @Override
            public String toString(GroupTableElement object) {
                return object.getNombre();
            }
            @Override
            public GroupTableElement fromString(String string) {
                return null;
            }
        });
        
        cbx_prod_lab.setConverter(new StringConverter<LabTableElement>(){
            @Override
            public String toString(LabTableElement object) {
                return object.getNombre();
            }
            @Override
            public LabTableElement fromString(String string) {
                return null;
            }
        });
        
        //COmbo box roles
        txt_u_rol.getItems().add("user");
        txt_u_rol.getItems().add("admin");
        
        //--------------- Desconectar de postgresql
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override public void run() {
                if(dbManager!=null){
                    InvTools.saveServerIp(txt_servidor.getText());
                    dbManager.desconectar();
                }
            }
        });
        
        //generarGrafica();
    }   
    
    @FXML  private void onCardClick(MouseEvent event) {
        MenuController.onCardClick(event, indicador, tabPane);
    }
    
    @FXML private void onBtnNotificacion(ActionEvent event) {
        if (menu_lateral.getTranslateX()>0) {
            menu_lateral.setTranslateX(0);
        }
        else{
            menu_lateral.setTranslateX(300);
        }
    }
        
    //---------------------------------------------------------------------------             Notificaciones
    @FXML private void onBtnAgregar(ActionEvent e){        
        FXMLLoader loader = new FXMLLoader(this.url);
        try {            
            AnchorPane pane = loader.load(getClass().getResourceAsStream("FXMLNotificacion.fxml"));
            notification_area.getChildren().add(pane);
            notification_area.setMargin(pane, new Insets(10,0,0,0));
            NotificationController controller = loader.getController();
            controller.lbl_titulo.setText("Hola " + notification_area.getChildren().size());           
        } catch (IOException ex) {
            Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //---------------------------------------------------------------------------             Inicio sesion
    @FXML
    private void onIniciarSesion(ActionEvent event) {
        
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText("Ups, a Warning Dialog");
        
        dbManager = new DbManager(txt_servidor.getText(), txt_nombre_base.getText(), "postgres", txt_clave_base.getText());
        
        if (dbManager.estado) {
            if (dbManager.getLogginInfo(txt_usuario.getText(), "admin", txt_contrasena.getText())) {
                panel_iniciar_sesion.setVisible(false);
                lbl_usuario.setText("Admin");
                lbl_estado_conexion.setText("En linea");
                led_estado.setFill(Color.GREEN);
                actualizarInfo();
                llenarListados();
                info_area.appendText(InvTools.getPersonalLog("Correcto ingreso"));
                
                //--Iniciar Thread
                ThreadTool local = new ThreadTool(this);
                local.start();
            }
            else{               
                alert.setContentText("Error al iniciar sesión");
                alert.showAndWait();
                dbManager.desconectar();
                info_area.appendText(InvTools.getPersonalLog("Error al ingresar"));
            }
        }
        else{
            alert.setHeaderText("Look, a Warning Dialog");
            alert.setContentText(dbManager.getError());
            alert.showAndWait();
        }
        
        
    }
    //------------------------------------------------------------------------------- TAB PRODUCTOS
    @FXML private void onProdEdit(MouseEvent e){
        ProductTableElement p = (ProductTableElement)tabla_productos.getSelectionModel().getSelectedItem();
        if (p != null && e.getClickCount()==2) {
            txt_prod_id.setText(p.getId()); 
            txt_prod_nom.setText(p.getNombre());
            cbx_prod_grup.getSelectionModel().select(listManager.getGroupFromName(p.getGrupo())); 
            txt_prod_stock.setText(p.getStock());
            txt_prod_pu.setText(p.getPrecio_unidad()); 
            txt_prod_pc.setText(p.getPrecio_compra()); 
            txt_prod_desc.setText(p.getDescripcion()); 
            date_prod_vence.setValue(LocalDate.parse(p.getFechaVencimiento()));
            cbx_prod_lab.getSelectionModel().select(listManager.getLabFromName(p.getLaboratorio()));
            txt_prod_porc.setText(p.getGanancia());
            txt_prod_codbar.setText(p.getCodigoBarras());
            txt_prod_lote.setText(p.getLote());
        }
        info_area.appendText(InvTools.getPersonalLog("Edición de productos"));
    }
    
    @FXML
    private void onProdGuardar(ActionEvent event) {
        if (txt_prod_nom.getText().length()==0||
            txt_prod_pu.getText().length()==0||
            date_prod_vence.getValue().toString().length()==0||
            cbx_prod_grup.getSelectionModel().getSelectedItem()== null){
            nuevoMensaje("", "Algunos campos estan sin llenar...");
            return;
        }
        
        ProductTableElement p = tabla_productos.getSelectionModel().getSelectedItem();
        
        if (txt_prod_id.getText().length()>0) {     
            //--TODO
            dbManager.editarProducto(txt_prod_id.getText(), txt_prod_nom.getText(), 
                    cbx_prod_grup.getSelectionModel().getSelectedItem().getId(), txt_prod_stock.getText(),
                    txt_prod_pu.getText(), txt_prod_pc.getText(), txt_prod_desc.getText(), date_prod_vence.getValue().toString(),
                    cbx_prod_lab.getSelectionModel().getSelectedItem().getId(), txt_prod_codbar.getText(), txt_prod_porc.getText(),txt_prod_lote.getText());
            
            p.actualizar(txt_prod_nom.getText(), cbx_prod_grup.getSelectionModel().getSelectedItem().getNombre(), 
                    txt_prod_stock.getText(), txt_prod_pu.getText(), txt_prod_pc.getText(), txt_prod_desc.getText(), 
                    date_prod_vence.getValue().toString(), cbx_prod_lab.getSelectionModel().getSelectedItem().getNombre(), 
                    txt_prod_codbar.getText(), txt_prod_porc.getText());
            tabla_productos.refresh();
        }
        else if(txt_prod_id.getText().length()==0){
            dbManager.nuevoProducto(txt_prod_nom.getText(), cbx_prod_grup.getSelectionModel().getSelectedItem().getId(), 
                    txt_prod_stock.getText(), txt_prod_pu.getText(), txt_prod_pc.getText(), txt_prod_desc.getText(), 
                    date_prod_vence.getValue().toString(),cbx_prod_lab.getSelectionModel().getSelectedItem().getId(),
                    txt_prod_codbar.getText(),txt_prod_porc.getText(), txt_prod_lote.getText());
            onProdTodo(null);
        }
        onProdCancelar(null);
        info_area.appendText(InvTools.getPersonalLog("Producto guardar"));
    }

    @FXML private void onProdCancelar(ActionEvent event) {
        txt_prod_id.setText("");
        txt_prod_nom.setText("");
        cbx_prod_grup.getSelectionModel().selectFirst(); 
        txt_prod_stock.setText("");
        txt_prod_pu.setText("");
        txt_prod_pc.setText("");
        txt_prod_desc.setText("");
        date_prod_vence.setValue(LocalDate.now());
        txt_prod_porc.setText("");
        txt_prod_codbar.setText("");
        txt_prod_sumarUni.setText("");
        txt_prod_lote.setText("");
        cbx_prod_lab.getSelectionModel().selectFirst();
    }

    @FXML private void onProdTyped(KeyEvent event) {
        listManager.searchProduct(txt_prod_search.getText());
    }

    @FXML private void onProdTodo(ActionEvent event) {
        listManager.setProductList(dbManager.getProductos());
        onProdCancelar(null);
        actualizarInfo();
        info_area.appendText(InvTools.getPersonalLog("Todos los productos."));
    }
    
    @FXML private void onProdEliminar(ActionEvent event) {
        ProductTableElement p = (ProductTableElement)tabla_productos.getSelectionModel().getSelectedItem();
        if (p != null) {
            if (!dbManager.eliminarProducto(p.getId())) {
                nuevoMensaje("", "El producto: "+p.getNombre() + " está en uso, por lo tanto no se puede eliminar");
            }else{
                listManager.getProductList().remove(p);
                actualizarInfo();
            } 
            onProdCancelar(null);
        }
        info_area.appendText(InvTools.getPersonalLog("Eliminar producto."));
    }

    //------------------------------------------------------------------------------- TAB USUARIOS
    
    @FXML private void onUserEdit(MouseEvent e) {
        UserTableElement u = (UserTableElement)tabla_usuarios.getSelectionModel().getSelectedItem();
        if (u != null && e.getClickCount()==2) {
            txt_u_nombres.setText(u.getNombre());
            txt_u_apellidos.setText(u.getApellidos());
            txt_u_cedula.setText(u.getCedula());
            txt_u_telefono.setText(u.getTelefono());
            txt_u_direccion.setText(u.getDireccion());
            txt_u_correo.setText(u.getCorreo());
            txt_u_password.setText(u.getPassword());
            txt_u_rol.getSelectionModel().select(u.getRol());
            //txt_u_rol.setText(u.getRol());
        }
        info_area.appendText(InvTools.getPersonalLog("Edicion de usuario"));
    }

    @FXML private void onUserSearch(KeyEvent event) {
        listManager.searchUser(txt_u_search.getText());
    }
    
    private void llenarListados(){
        listManager.setProductList(dbManager.getProductos());
        listManager.setUserList(dbManager.getUsuarios());
        
        //--Grupos
        listManager.setGroupList(dbManager.getGrupos());
        cbx_prod_grup.getItems().clear();
        cbx_prod_grup.getItems().addAll(listManager.getGroupList());
        info_area.appendText(InvTools.getPersonalLog("Llenando listados..."));
        
        listManager.setLabList(dbManager.getLaboratorios());
        cbx_prod_lab.getItems().clear();
        cbx_prod_lab.getItems().addAll(listManager.getLabList());    
    }

    @FXML private void onUsuarioGuardar(ActionEvent e) {        
        if (txt_u_cedula.getText().length()==0||
            txt_u_nombres.getText().length()==0||
            txt_u_rol.getSelectionModel().getSelectedItem()==null||
            txt_u_password.getText().length()==0){
            nuevoMensaje("", "Algunos importantes campos estan sin llenar...");
            return;
        }       
        
        UserTableElement u = null;
        boolean found = false;
        for (int i = 0; i < tabla_usuarios.getItems().size(); i++) {
            u = tabla_usuarios.getItems().get(i);
            if (u.getCedula().compareTo(txt_u_cedula.getText())==0) {
                found = true;
                break;
            }
        }               
        
        if (u!=null && found) {
            dbManager.editarUsuario(txt_u_cedula.getText(), txt_u_nombres.getText(), 
                    txt_u_apellidos.getText(), txt_u_telefono.getText(), txt_u_direccion.getText(), 
                    txt_u_correo.getText(), txt_u_rol.getSelectionModel().getSelectedItem(), txt_u_password.getText());
            
            u.actualizar(txt_u_cedula.getText(), txt_u_nombres.getText(), 
                    txt_u_apellidos.getText(), txt_u_telefono.getText(), txt_u_direccion.getText(), 
                    txt_u_correo.getText(), txt_u_rol.getSelectionModel().getSelectedItem(), txt_u_password.getText());
            tabla_usuarios.refresh();
        }
        else{
            if (!dbManager.nuevoUsuario(txt_u_cedula.getText(), txt_u_nombres.getText(), 
                    txt_u_apellidos.getText(), txt_u_telefono.getText(), txt_u_direccion.getText(), 
                    txt_u_correo.getText(), txt_u_rol.getSelectionModel().getSelectedItem(), txt_u_password.getText())) {
                nuevoMensaje("", "Error al ingresar el nuevo usuario");
            }      
            onUserTodo(null);
        }
        onGrupCancelar(null);
        info_area.appendText(InvTools.getPersonalLog("Nuevo usuario"));
    }
    
    @FXML
    private void onUserTodo(ActionEvent event) {
        listManager.setUserList(dbManager.getUsuarios());      
        onUserCancel(null);
        info_area.appendText(InvTools.getPersonalLog("Todos los usuarios"));
        actualizarInfo();
    }

    @FXML  private void onUserCancel(ActionEvent event) {
        txt_u_cedula.setText("");
        txt_u_nombres.setText(""); 
        txt_u_apellidos.setText("");
        txt_u_telefono.setText("");
        txt_u_direccion.setText("");
        txt_u_correo.setText("");
        txt_u_rol.getSelectionModel().select("user");
        txt_u_password.setText("");
    }
    
    
    public void actualizarInfo(){
        lbl_productos_registrados.setText(dbManager.getTotalProductos()+"");
        lbl_ventas_nuevas.setText(dbManager.getTotalVentasHoy()+"");
        lbl_total_usuarios.setText(dbManager.getTotalUsuarios()+"");
    }
    
        
    private void nuevoMensaje(String titulo, String info){
        Alert alert = new Alert(AlertType.WARNING);
        alert.setTitle("Warning Dialog");
        alert.setHeaderText("Ups, a Warning Dialog");
        alert.setContentText(info);
        alert.showAndWait();
        info_area.appendText(InvTools.getPersonalLog("Se recibió una nueva alerta"));
    }

    @FXML private void onGrupGuardar(ActionEvent event) {
        if (txt_grup_nom.getText().length()==0||
            txt_grup_desc.getText().length()==0){
            nuevoMensaje("", "Algunos campos estan sin llenar...");
            return;
        }
        
        GroupTableElement g = tabla_grupos.getSelectionModel().getSelectedItem();
        
        if (txt_grup_id.getText().length()>0) {                        
            dbManager.editarGrupo(txt_grup_id.getText(), txt_grup_nom.getText(), txt_grup_desc.getText());            
            g.actualizar(txt_grup_id.getText(), txt_grup_nom.getText(), txt_grup_desc.getText());            
            tabla_grupos.refresh();
        }
        else if(txt_grup_id.getText().length()==0){
            dbManager.nuevoGrupo(txt_grup_nom.getText(), txt_grup_desc.getText());                    
            onGrupTodo(null);
        }
        onGrupCancelar(null);
        info_area.appendText(InvTools.getPersonalLog("Guardar grupo"));
    }

    @FXML
    private void onGrupCancelar(ActionEvent event) {
        txt_grup_id.setText("");
        txt_grup_nom.setText("");
        txt_grup_desc.setText("");
        cbx_prod_grup.getItems().clear();
        cbx_prod_grup.getItems().addAll(listManager.getGroupList());
    }
    
    private void onGrupTodo(ActionEvent event) {
        listManager.setGroupList(dbManager.getGrupos());        
        onGrupCancelar(null);
        actualizarInfo();
        info_area.appendText(InvTools.getPersonalLog("Todos los grupos"));
    }
    
    @FXML private void onGroupEdit(MouseEvent e) {
        GroupTableElement g = tabla_grupos.getSelectionModel().getSelectedItem();
        if (g != null && e.getClickCount()==2) {
            txt_grup_id.setText(g.getId());
            txt_grup_nom.setText(g.getNombre());
            txt_grup_desc.setText(g.getDescripcion());
        }
        info_area.appendText(InvTools.getPersonalLog("Edicion grupo"));
    }

    @FXML  private void onGrupEliminar(ActionEvent event) {
        GroupTableElement g = tabla_grupos.getSelectionModel().getSelectedItem();
        if (g != null) {
            if (!dbManager.eliminarGrupo(g.getId())) {
                nuevoMensaje("", "El grupo "+g.getNombre()+" está siendo usado, no se puede eliminar");
            }
            else{
                listManager.getGroupList().remove(g);
                actualizarInfo();
                onGrupCancelar(null);
            }            
        }
        info_area.appendText(InvTools.getPersonalLog("Eliminar grupo"));
    }

    @FXML
    private void onUserEliminar(ActionEvent event) {
        UserTableElement u = tabla_usuarios.getSelectionModel().getSelectedItem();
        if (u != null) {
            if (!dbManager.eliminarUsuario(u.getCedula())) {
                nuevoMensaje("", "El usuario "+u.getNombre()+" está siendo usado, no se puede eliminar");
            }
            else{
                listManager.getUserList().remove(u);
                actualizarInfo();
                onUserCancel(null);
            }            
        }
        info_area.appendText(InvTools.getPersonalLog("Eliminar ventas"));
    }

    @FXML
    private void onVentasTodo(ActionEvent event) {
        //--Actualizar listados vinculados a las tablas
        ObservableList<PieChart.Data> data= dbManager.ventasPorLab();
        listManager.setSalesList(dbManager.getVentas());
        listManager.setSalesLabList(data);
        //--Actualizar los labels
        lbl_sales_total_ventas.setText(tabla_ventas.getItems().size()+"");
        lbl_sales_valor_ventas.setText(InvTools.getValorVentas(tabla_ventas.getItems())+"");
        lbl_sales_ganancias.setText(InvTools.getGananciasVentas(tabla_ventas.getItems())+"");
        
        //--Codigo agregado, resultados y graficacion
        InvTools.setChartInfo(chart_ventas, dbManager.getConteoVentas());
        InvTools.setChartInfo(chart_laboratorios, data);
        info_area.appendText(InvTools.getPersonalLog("Total ventas"));
    }

    @FXML
    private void onVentasConsultar(ActionEvent event) {
        if (date_ventas_inicio.getValue() != null && date_ventas_final.getValue() != null) {
            String fecha_inicio = date_ventas_inicio.getValue().toString();
            String fecha_final = date_ventas_final.getValue().toString();
            
            ObservableList<PieChart.Data> data= dbManager.ventasPorLab(fecha_inicio, fecha_final);
            
            //--Actualizar listados vinculados a las tablas            
            listManager.setSalesList(dbManager.getVentas(fecha_inicio, fecha_final));
            listManager.setSalesLabList(data);
            
            //--Codigo agregado, resultados y graficacion
            lbl_sales_total_ventas.setText(tabla_ventas.getItems().size()+"");
            lbl_sales_valor_ventas.setText(InvTools.getValorVentas(tabla_ventas.getItems())+"");
            lbl_sales_ganancias.setText(InvTools.getGananciasVentas(tabla_ventas.getItems())+"");
            
            InvTools.setChartInfo(chart_ventas, dbManager.getConteoVentas(fecha_inicio, fecha_final));
            InvTools.setChartInfo(chart_laboratorios, data);
        }
        else{
            nuevoMensaje("", "Debe seleccionar las fechas...");
        }
        info_area.appendText(InvTools.getPersonalLog("Total ventas en periodo especifico."));
    }

    @FXML
    private void onSalesEliminar(ActionEvent event) {
        SalesTableElement s = tabla_ventas.getSelectionModel().getSelectedItem();
        if (s != null) {
            if (dbManager.eliminarVenta(s.getId())) {
                tabla_ventas.getItems().remove(s);
            }
            else{
                nuevoMensaje("", "No se pudo eliminar esta venta...\n" + dbManager.getError());
            }
        }
        info_area.appendText(InvTools.getPersonalLog("Eliminar ventas."));
    }
    
    //-----------------------------------------------------------------------    Validacones y creacion de alertas
    
    public void nuevaNotificacion(String icono, String titulo, String info){
        FXMLLoader loader = new FXMLLoader(this.url);
        try {
            AnchorPane pane = loader.load(getClass().getResourceAsStream("FXMLNotificacion.fxml"));
            notification_area.getChildren().add(pane);
            notification_area.setMargin(pane, new Insets(10,0,0,0));
            NotificationController controller = loader.getController();
            controller.lbl_titulo.setText(titulo);     
            controller.lbl_informacion.setText(info);
        } catch (IOException ex) {
            Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void validarVencimiento(){
        actualizarInfo();
        for (ProductTableElement i : tabla_productos.getItems()) {
            if (InvTools.getDaysInRange(i.getFechaVencimiento())<20) {
                nuevaNotificacion("", "Alerta de vencimiento ", "El producto " + i.getNombre() + " está por caducar");
            }
        }
        info_area.appendText(InvTools.getPersonalLog("Revisión automática para validar fecha de vencimiento"));
    }
    
    public void validarStock(){
        actualizarInfo();
        for (ProductTableElement i : tabla_productos.getItems()) {
            if (Integer.parseInt(i.getStock())<20) {
                nuevaNotificacion("", "Alerta de stock ", "Pocas unidades de " + i.getNombre());
            }
        }
        info_area.appendText(InvTools.getPersonalLog("Revisión automática para validar stock"));
    }
    
    
    @FXML private void onBackup(ActionEvent event){        
        DirectoryChooser dc = new DirectoryChooser();
        dc.setTitle("Seleccione una carpeta para guardar archivos");
        File selectedDirectory = dc.showDialog(((Node) event.getSource()).getScene().getWindow());

        if(selectedDirectory != null){
            info_area.appendText(InvTools.getPersonalLog("Creando copia de seguridad, tarea ejecutada por el usuario."));
            dbManager.makeBackup(selectedDirectory.getAbsolutePath());
        }
    }
//-------------------------------------------------------------------------------Laboratorios
    @FXML private void onLabGuardar(ActionEvent event) {
        if (txt_lab_nombre.getText().length()==0){
            nuevoMensaje("", "Algunos campos estan sin llenar...");
            return;
        }
        
        LabTableElement l = tabla_laboratorios.getSelectionModel().getSelectedItem();
        
        if (txt_prod_id.getText().length()>0) {     
       
            dbManager.editarLaboratorio(txt_lab_id.getText(), txt_lab_nombre.getText(),
                    txt_lab_telefono.getText() , txt_lab_correo.getText(), 
                    txt_lab_direccion.getText(), txt_lab_direccion.getText());
            
            l.actualizar(txt_lab_nombre.getText(),
                    txt_lab_telefono.getText() , txt_lab_correo.getText(), 
                    txt_lab_direccion.getText(), txt_lab_direccion.getText());
            tabla_productos.refresh();
        }
        else if(txt_lab_id.getText().length()==0){
            dbManager.nuevoLaboratorio(txt_lab_nombre.getText(), txt_lab_telefono.getText(), txt_lab_correo.getText(),txt_lab_direccion.getText(), txt_lab_descripcion.getText());
            onLabTodo(null);
        }
        onLabCancelar(null);
        info_area.appendText(InvTools.getPersonalLog("Laboratorio guardar"));
    }

    @FXML private void onLabCancelar(ActionEvent event) {
        txt_lab_id.setText("");
        txt_lab_nombre.setText("");
        txt_lab_telefono.setText("");
        txt_lab_direccion.setText("");
        txt_lab_descripcion.setText("");
        txt_lab_correo.setText("");
        cbx_prod_lab.getItems().clear();
        cbx_prod_lab.getItems().addAll(listManager.getLabList());
    }

    @FXML private void onLabEliminar(ActionEvent event) {
        LabTableElement l = tabla_laboratorios.getSelectionModel().getSelectedItem();
        if (l != null) {
            if (!dbManager.eliminarLaboratorio(l.getId())) {
                nuevoMensaje("", "El laboratorio "+l.getNombre()+" no se puede eliminar porque está en uso...");
            }else{
                listManager.getLabList().remove(l);
                actualizarInfo();
            } 
            onLabCancelar(null);
        }
        info_area.appendText(InvTools.getPersonalLog("Eliminar laboratorio."));
    }
    
    private void onLabTodo(ActionEvent event) {
        //listManager.setGroupList(dbManager.getGrupos());    
        listManager.setLabList(dbManager.getLaboratorios());
        onLabCancelar(null);
        actualizarInfo();
        info_area.appendText(InvTools.getPersonalLog("Visualizando todos los laboratorios"));
    }
    
    @FXML private void onLabEdit(MouseEvent e) {
        LabTableElement l = tabla_laboratorios.getSelectionModel().getSelectedItem();
        if (l != null && e.getClickCount()==2) {
            txt_lab_id.setText(l.getId());
            txt_lab_nombre.setText(l.getNombre());
            txt_lab_telefono.setText(l.getTelefono());
            txt_lab_direccion.setText(l.getDireccion());
            txt_lab_descripcion.setText(l.getDescripcion());
            txt_lab_correo.setText(l.getCorreo());
        }
        info_area.appendText(InvTools.getPersonalLog("Edicion laboratorio"));
    }
    
    @FXML private void onLabTyped(KeyEvent event) {
        listManager.searchLaboratory(txt_lab_search.getText());
    }

    @FXML private void onPorcentaje(ActionEvent event) {
        txt_prod_pu.setText(InvTools.calcularPrecio(txt_prod_pc.getText(), txt_prod_porc.getText()));
    }

    @FXML
    private void prodTableKeyPressed(KeyEvent event) {
        if (event.getCode() == KeyCode.DELETE) {
            onProdEliminar(null);
        }
    }

    @FXML private void onSumarUnidades(ActionEvent event) {
        if (txt_prod_stock.getText().length()>0 && txt_prod_sumarUni.getText().length()>0) {
            int total = Integer.parseInt(txt_prod_stock.getText());
            total += Integer.parseInt(txt_prod_sumarUni.getText());
            txt_prod_stock.setText(total+"");
        }
    }
    
    
    @FXML private void onRestore(ActionEvent event) {
        
    }
}
