/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemainvetario.conexiones;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.PieChart;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;
import sistemainvetario.TabElements.GroupTableElement;
import sistemainvetario.TabElements.LabTableElement;
import sistemainvetario.TabElements.ProductTableElement;
import sistemainvetario.TabElements.SalesTableElement;
import sistemainvetario.TabElements.UserTableElement;

/**
 * @author Luis Espana<fdh.luis.angel@gmail.com>
 */
public class DbManager {
    public static String PRODUCTOS = ("select id_producto, productos.nombre as nom_producto, grupos.nombre as nom_grupo, \n" +
"stock, precio_unidad, precio_compra, productos.descripcion, fecha_vencimiento, \n" +
"laboratorios.nombre as laboratorio, codigo_barras, pganancia, lote\n" +
"from productos join grupos on productos.id_grupo = grupos.id_grupo\n" +
"join laboratorios on laboratorios.cod_laboratorio = productos.cod_laboratorio;");
    
    private static String USUARIOS = ("select nombres, apellidos, cedula, telefono, correo, direccion, rol, usuarios.password as contrasena from usuarios;");
    
    private static String GRUPOS = "select * from grupos";
    
    private static String VENTAS = ("select id_venta, productos.nombre, unidades, usuarios.nombres as usuario, \n" +
"fecha, valor, ganancia, laboratorios.nombre as laboratorio\n" +
"from ventas join usuarios on ventas.cedula = usuarios.cedula \n" +
"join laboratorios on ventas.cod_laboratorio = laboratorios.cod_laboratorio\n" +
"join productos on ventas.id_producto = productos.id_producto");
    
    public static String LOGGININFO = ("select cedula, rol, usuarios.password as contrasena from usuarios;");
    
    public static String TOTAL_PRODUCTOS = ("select count('id_producto')as total_productos from productos;");
    
    private static String TOTAL_USUARIOS = ("select count('cedula') as total_usuarios from usuarios;");   
    
    
    
    private Connection conn = null;
    private String ipServer;
    private String dbName;
    private String dbUser;
    private String dbPassword;
    private String urlDatabase;
    public boolean estado = false;
    private String error = "";

    public DbManager(String ipServer, String dbName, String dbUser, String dbPassword) {
        this.ipServer = ipServer;
        this.dbName = dbName;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
        this.urlDatabase = String.format("jdbc:postgresql://%s:5432/%s",ipServer, dbName);
        System.out.println(urlDatabase);
        conectar();
    }
    
    public void conectar(){ 
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection(urlDatabase,  dbUser, dbPassword);
        } catch (Exception e) {
            error = e.getMessage();
            System.out.println("Ocurrio un error : "+e.getMessage());
        }     
        
        if (conn!=null) {
            estado = true;
        }
    }
    //------------------------------------------------------------- PRODUCTOS
    public ObservableList getProductos(){
        ObservableList<ProductTableElement> lstProd = FXCollections.observableArrayList();
        if (conn != null) {
            try {
                Statement st = conn.createStatement();
                
                ResultSet rs = st.executeQuery( DbManager.PRODUCTOS );
                while ( rs.next() ) {
                    int id_producto = rs.getInt("id_producto");
                    String  nom_producto = rs.getString("nom_producto");
                    String  nom_grupo = rs.getString("nom_grupo");
                    int stock = rs.getInt("stock");
                    int precio_unidad = rs.getInt("precio_unidad");
                    int precio_compra = rs.getInt("precio_compra");
                    String descripcion = rs.getString("descripcion");
                    Date fecha_vencimiento = rs.getDate("fecha_vencimiento");                    
                    String laboratorio = rs.getString("laboratorio");
                    String codigoB = rs.getString("codigo_barras");
                    String pganancia = rs.getString("pganancia");
                    String lote = rs.getString("lote");
                    ProductTableElement p = new ProductTableElement(id_producto+"", 
                            nom_producto, nom_grupo, stock+"", precio_unidad+"", 
                            precio_compra+"", descripcion, fecha_vencimiento.toString(), 
                            laboratorio, codigoB, pganancia, lote);
                    lstProd.add(p);
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lstProd;
    }
    
    public int getTotalProductos(){
        int count = 0;
        
        if (conn!=null) {
            try {
                Statement st = conn.createStatement();
                
                ResultSet rs = st.executeQuery( DbManager.TOTAL_PRODUCTOS );
                while ( rs.next() ) {
                    count = rs.getInt("total_productos");
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
        return count;
    }
    
    
    
    public boolean editarProducto(String cod, String nom, String id_grup, String stock, String pv, String pc, String desc, String fechaVen, String lab, String codbar, String pganan, String lote){
        String query = String.format("select editarProducto(%s, '%s', %s, %s, %s, %s, '%s', '%s','%s','%s','%s','%s');",cod, nom,id_grup, stock, pv, pc, desc, fechaVen,lab,codbar,pganan, lote);
    
        if (conn!=null) {
            try {
                Statement st = conn.createStatement();                
                st.executeQuery(query);                             
                st.close();
                return true;
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    
    public boolean nuevoProducto(String nombre,String  id_grupo,String  stock,String  precio_unidad,String  precio_compra, String descripcion, String fecha_vencimiento, 
            String cod_lab,String cod_bar ,String pganan, String lote){
        String query = String.format("insert into productos(nombre, id_grupo, stock, precio_unidad, precio_compra, descripcion, fecha_vencimiento,cod_laboratorio, codigo_barras,pganancia, lote) values\n" +
        "('%s', %s , %s, %s, %s, '%s', '%s', '%s', '%s', '%s', '%s')", 
        nombre, id_grupo, stock, precio_unidad, precio_compra, descripcion, fecha_vencimiento, cod_lab, cod_bar, pganan, lote);
        return ejecutarQuery(query, true);
    }
    
    public boolean eliminarProducto(String cod_producto){
        String query = String.format("delete from productos where id_producto = %s;", cod_producto);
        return ejecutarQuery(query, true);
    }
    
    //----------------------------------------------------------------USUARIOS
    public int getTotalUsuarios(){
        int count = 0;
        
        if (conn!=null) {
            try {
                Statement st = conn.createStatement();
                
                ResultSet rs = st.executeQuery( DbManager.TOTAL_USUARIOS );
                while ( rs.next() ) {
                    count = rs.getInt("total_usuarios");
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
        return count;
    }
    
    public ObservableList getUsuarios(){
        ObservableList<UserTableElement> lstUsu = FXCollections.observableArrayList();
        if (conn != null) {
            try {
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery( DbManager.USUARIOS );
                while ( rs.next() ) {
                   
                    String  nombres = rs.getString("nombres");
                    String  apellidos = rs.getString("apellidos");
                    String cedula = rs.getString("cedula");
                    String  telefono = rs.getString("telefono");
                    String correo = rs.getString("correo");
                    String  direccion = rs.getString("direccion");
                    String rol = rs.getString("rol");              
                    String contrasena = rs.getString("contrasena");                          
                    
                    UserTableElement u = new UserTableElement(nombres, apellidos, cedula, telefono, correo, direccion, rol, contrasena);
                    lstUsu.add(u);
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lstUsu;
    }
    
    public boolean editarUsuario(String cedula, String nombres, String apellidos, String telefono, String dir, String mail, String rol, String clave){
        String query = String.format("select editarusuario('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');", cedula, nombres, apellidos, telefono, dir, mail, rol, clave);
        return ejecutarQuery(query, false);
    }
    
    public boolean nuevoUsuario(String cedula, String nombres, String apellidos, String telefono, String direccion, String correo, String rol, String password){
        String query = String.format("insert into usuarios(cedula, nombres, apellidos, telefono, direccion, correo, rol, password) values\n" +
        "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');", 
        cedula, nombres, apellidos, telefono, direccion, correo, rol, password);
        return ejecutarQuery(query, true);
    }
    
    public boolean eliminarUsuario(String cedula){
        String query = String.format("delete from usuarios where cedula like '%s';", cedula);
        return ejecutarQuery(query, true);
    }
    //-----------------------------------------------------------------VENTAS
    public int getTotalVentasHoy(){
        int count = 0;
        String query = String.format("select count('id_venta') as total_ventas from ventas where fecha > '%s';", java.time.LocalDate.now().toString());
        
        if (conn!=null) {
            try {
                Statement st = conn.createStatement();
                
                ResultSet rs = st.executeQuery( query );
                while ( rs.next() ) {
                    count = rs.getInt("total_ventas");
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
        return count;
    }
    
        
    public boolean vender(String query){
        return ejecutarQuery(query, false);
    }
    
    public ObservableList getVentas(){
        ObservableList<SalesTableElement> lstVentas = FXCollections.observableArrayList();
        if (conn != null) {
            try {
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery( DbManager.VENTAS );
                while ( rs.next() ) {
                   
                    int  id_v = rs.getInt("id_venta");
                    String  nom = rs.getString("nombre");
                    String uni = rs.getString("unidades");
                    String  usu = rs.getString("usuario");
                    Date fech = rs.getTimestamp("fecha");
                    int  valor = rs.getInt("valor");
                    int  ganancia = rs.getInt("ganancia");
                    String  laboratorio = rs.getString("laboratorio");  
                    
                    
                    SalesTableElement s = new SalesTableElement(id_v+"", nom, uni, usu, fech.toString(), valor+"", ganancia+"", laboratorio);
                    lstVentas.add(s);
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lstVentas;
    }
    
    public ObservableList getVentas(String fecha_inicio, String fecha_final){
        String query = String.format(" where fecha >= '%s' and fecha <= DATE('%s')+CAST('1 days' as interval) order by fecha;", fecha_inicio, fecha_final);
        
        ObservableList<SalesTableElement> lstVentas = FXCollections.observableArrayList();
        if (conn != null) {
            try {
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery(VENTAS + query );
                //System.out.println("Query ventas en periodo especifico:\n "+VENTAS + query);
                while ( rs.next() ) {
                   
                    int  id_v = rs.getInt("id_venta");
                    String  nom = rs.getString("nombre");
                    String uni = rs.getString("unidades");
                    String  usu = rs.getString("usuario");
                    Date fech = rs.getTimestamp("fecha");
                    int  valor = rs.getInt("valor");
                    int  ganancia = rs.getInt("ganancia");
                    String  laboratorio = rs.getString("laboratorio");   
                    
                    SalesTableElement s = new SalesTableElement(id_v+"", nom, uni, usu, fech.toString(), valor+"", ganancia+"", laboratorio);
                    lstVentas.add(s);
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lstVentas;
    }
    
    public boolean eliminarVenta(String id_venta){
        String query = String.format("delete from ventas where id_venta = %s", id_venta);
        return ejecutarQuery(query, true);
    }
    
    //------------------------------------------------------------------ GRUPOS
    public  boolean editarGrupo(String id, String nom, String desc) {
        String query = String.format("select editarGrupo(%s,'%s', '%s');", id, nom, desc);
        return ejecutarQuery(query, false);
    }
    
    
    public ObservableList getGrupos(){
        ObservableList<GroupTableElement> lstGrupos = FXCollections.observableArrayList();
        if (conn != null) {
            try {
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery( DbManager.GRUPOS );
                while ( rs.next() ) {                   
                    int  id = rs.getInt("id_grupo");
                    String  nom = rs.getString("nombre");
                    String desc = rs.getString("descripcion");
                    
                    GroupTableElement g = new GroupTableElement(id+"", nom, desc);
                    lstGrupos.add(g);
                    //System.out.println(String.format("%d, %s, %s", id, nom, desc));
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lstGrupos;
    }
    
    public boolean nuevoGrupo(String nombre, String descripcion){
        String query = String.format("insert into grupos(nombre, descripcion) values('%s', '%s');", nombre, descripcion);
        return ejecutarQuery(query, true);
    }
    
    public boolean eliminarGrupo(String id_grupo){
        String query = String.format("delete from grupos where id_grupo = %s;", id_grupo);
        return ejecutarQuery(query, true);
    }
    
    
    public boolean getLogginInfo(String cedula, String rol, String contrasena){
        boolean found = false;
        if (conn!=null) {
            try {
                Statement st = conn.createStatement();
                
                ResultSet rs = st.executeQuery( DbManager.LOGGININFO );
                while ( rs.next() ) {                    
                    String  bcedula = rs.getString("cedula");
                    String  brol = rs.getString("rol");
                    String bcon = rs.getString("contrasena");  
                    if (bcedula.compareTo(cedula)==0 && brol.compareTo(rol)==0 && bcon.compareTo(contrasena)==0) {
                        found = true;
                        break;
                    }
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return found;
    }
    
    
    
    
    public void desconectar(){
        System.out.println("Desconectando....");
        if (conn == null) return;
        try {
            conn.close();
        } catch (SQLException ex) {
            error = ex.getMessage();
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getError(){
        return error;
    }
    
    private boolean ejecutarQuery(String query, boolean basica){        
        if (conn!=null) {
            try {
                Statement st = conn.createStatement();
                if (basica) {
                    st.executeUpdate(query);
                }
                else{
                    st.executeQuery(query);
                }                         
                st.close();
                return true;
            } catch (SQLException ex) {
                error = ex.getMessage();                
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);                
            }
        }
        return false;
    }
    
    //-- Conteo de ventas
    public LinkedList<String> getConteoVentas(){
        String query =  "select nombres, sum(unidades) as num_ventas, date(fecha) as mi_fecha\n" +
                        "from ventas natural join usuarios group by 1, 3 order by mi_fecha;";
        LinkedList<String> info = new LinkedList<>();
        if (conn!=null) {
            try {
                Statement st = conn.createStatement();                
                ResultSet rs = st.executeQuery( query );
                while ( rs.next() ) {                    
                    String  nombres = rs.getString("nombres");
                    int ventas = rs.getInt("num_ventas");
                    String fecha = rs.getString("mi_fecha");
                    
                    info.add(String.format("%s;%d;%s", nombres, ventas, fecha));
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return info;
    }
    
    //-- Conteo de ventas
    public LinkedList<String> getConteoVentas(String fecha_inicial, String fecha_final){
        String c =  ("select nombres, sum(unidades) as num_ventas, date(fecha) as mi_fecha \n" +
                        "from ventas natural join usuarios \n" +
                        "where fecha >= '%s' and fecha <= DATE('%s')+CAST('1 days' as interval)\n" +
                        "group by 1, 3 order by mi_fecha;");
        String query = String.format(c, fecha_inicial, fecha_final);
        LinkedList<String> info = new LinkedList<>();
        if (conn!=null) {
            try {
                Statement st = conn.createStatement();                
                ResultSet rs = st.executeQuery( query );
                while ( rs.next() ) {                    
                    String  nombres = rs.getString("nombres");
                    int ventas = rs.getInt("num_ventas");
                    String fecha = rs.getString("mi_fecha");
                    
                    info.add(String.format("%s;%d;%s", nombres, ventas, fecha));
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return info;
    }
    //------------------ Conteo de numero de ventas por Laboratorio
    public ObservableList<PieChart.Data> ventasPorLab(){
        String query =  "select sum(unidades) as total, laboratorios.nombre from ventas \n" +
                        "natural join laboratorios group by laboratorios.nombre\n" +
                        "order by total DESC;";
        
        ObservableList<PieChart.Data> info = FXCollections.observableArrayList();
        if (conn!=null) {
            try {
                Statement st = conn.createStatement();                
                ResultSet rs = st.executeQuery( query );
                while ( rs.next() ) {                    
                    int total = rs.getInt("total");
                    String nombre = rs.getString("nombre");
                    
                    info.add(new PieChart.Data(nombre, total));
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return info;
    }
    
    public ObservableList<PieChart.Data> ventasPorLab(String fecha_inicio, String fecha_final){
        String query = String.format("select sum(unidades) as total, laboratorios.nombre from ventas natural join laboratorios\n" +
        "where fecha >= '%s' and fecha <= DATE('%s')+CAST('1 days' as interval)\n" +
        "group by laboratorios.nombre\n" +
        "order by total DESC;", fecha_inicio, fecha_final);
        
        ObservableList<PieChart.Data> info = FXCollections.observableArrayList();
        if (conn!=null) {
            try {
                Statement st = conn.createStatement();                
                ResultSet rs = st.executeQuery( query );
                while ( rs.next() ) {                    
                    int total = rs.getInt("total");
                    String nombre = rs.getString("nombre");
                    
                    info.add(new PieChart.Data(nombre, total));
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return info;
    }
    
    
    //--------------------------------- ---------------------------------backups
    private void backup(String ruta, String file_name, String query){
        try {
            CopyManager copyManager = new CopyManager((BaseConnection) conn);
            File file = new File(ruta + "/" + file_name +".csv");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            
            copyManager.copyOut("COPY (" + query + ") TO STDOUT WITH (FORMAT CSV, HEADER, DELIMITER ';')", fileOutputStream);            
            fileOutputStream.close();
        } catch (SQLException ex) {
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void makeBackup(String ruta){
        backup(ruta, "ventas", "select * from ventas");
        backup(ruta, "productos", "select * from productos");
        backup(ruta, "usuarios", "select * from usuarios");
        backup(ruta, "grupos", "select * from grupos");
    }
    
    public boolean editarLaboratorio(String id,String nombre,String telefono,String correo, String direccion,String descripcion){
        String query = String.format("select editarLaboratorio(%s, '%s', '%s', '%s', '%s' ,'%s');", id, nombre, telefono, correo, direccion, descripcion);
        return ejecutarQuery(query, false);
    }
    
    public boolean nuevoLaboratorio(String nombre,String telefono,String correo,String direccion,String descripcion){
        String query = String.format("insert into laboratorios(nombre, telefono, correo, direccion, descripcion) values ('%s','%s','%s','%s','%s');",
        nombre,telefono,correo, direccion, descripcion);
        return ejecutarQuery(query, true);
    }
    
    
    public ObservableList<LabTableElement> getLaboratorios() {
        String query = String.format("Select * from laboratorios;");
        
        ObservableList<LabTableElement> lstLabs = FXCollections.observableArrayList();
        if (conn != null) {
            try {
                Statement st = conn.createStatement();
                ResultSet rs = st.executeQuery( query );
                while ( rs.next() ) {
                   
                    int  id = rs.getInt("cod_laboratorio");
                    String nombre = rs.getString("nombre");
                    String telefono = rs.getString("telefono");
                    String correo = rs.getString("correo");
                    String direccion = rs.getString("direccion");
                    String descripcion = rs.getString("descripcion");
                                           
                    LabTableElement l = new LabTableElement(id+"", nombre, telefono, correo, direccion, descripcion);                    
                    lstLabs.add(l);
                }
                rs.close();                
                st.close();
            } catch (SQLException ex) {
                error = ex.getMessage();
                Logger.getLogger(DbManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lstLabs;
    }
    
    public boolean eliminarLaboratorio(String cod_lab){
        String query = String.format("delete from laboratorios where cod_laboratorio = %s;", cod_lab);
        return ejecutarQuery(query, true);
    }
    
    
    public static void main(String[] args) {
        
        DbManager dbManager = new DbManager("172.17.198.87", "dbsanar", "postgres", "1234");
        
        /*System.out.println("Total pro " + dbManager.getTotalProductos());
        System.out.println("Total usu " + dbManager.getTotalUsuarios());
        System.out.println("Total ventas hoy " + dbManager.getTotalVentasHoy());
        */
        //System.out.println("Usuarios....");
        //System.out.println("Editar ....");
            //dbManager.editarUsuario("1085320429", "Nom usu", "Ape usu", "789456123", "dir", "@.com", "admin", "1234");
            //dbManager.getUsuarios();\
            //dbManager.editarProducto("6", "Dolex", "2", "150", "1800", "1600", "Para la fiebre", "2020-3-3");
            //dbManager.editarGrupo("2", "ASO", "Lo mismo pero mas barato");
            //dbManager.getGrupos();
            //dbManager.getVentas();
            /*LinkedList<String> res =  dbManager.getConteoVentas();
            for (int i = 0; i < res.size(); i++) {
            System.out.println(res.get(i));
            }*/
        //dbManager.getUsuarios();
        //dbManager.makeBackup("");
        
        System.out.println(dbManager.ventasPorLab("2020-01-26", "2020-01-26"));       
        System.out.println(dbManager.ventasPorLab());
        
        dbManager.desconectar();
    }
}
