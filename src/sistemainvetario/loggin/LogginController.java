
package sistemainvetario.loggin;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;
import sistemainvetario.fxventas.FxVentas;
import sistemainvetario.inventario.SistemaInvetario;

/**
 *
 * @author LuisServer
 */
public class LogginController implements Initializable {
            
        
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }   

    @FXML private void onAdministrador(ActionEvent event) throws Exception {
        Platform.runLater(new Runnable() {
            @Override public void run() {		
                try {
                    new SistemaInvetario().start(new Stage());
                } catch (Exception ex) {
                    Logger.getLogger(LogginController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
	});
    }

    @FXML private void onVendedor(ActionEvent event) {
        Platform.runLater(new Runnable() {
            @Override public void run() {		
                try {
                    new FxVentas().start(new Stage());
                } catch (Exception ex) {
                    Logger.getLogger(LogginController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
	});
    }
}
