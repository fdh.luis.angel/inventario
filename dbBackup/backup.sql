--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.20
-- Dumped by pg_dump version 11.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: editargrupo(integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.editargrupo(id_grup integer, nom character varying, descrip character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE grupos set nombre=nom where id_grup = id_grupo;
	UPDATE grupos set descripcion=descrip where id_grup = id_grupo;
END;
$$;


ALTER FUNCTION public.editargrupo(id_grup integer, nom character varying, descrip character varying) OWNER TO postgres;

--
-- Name: editarlaboratorio(integer, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.editarlaboratorio(cod_lab integer, nom character varying, tel character varying, cor character varying, direc character varying, descrip character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE laboratorios set nombre=nom where cod_laboratorio = cod_lab;
	UPDATE laboratorios set telefono=tel where cod_laboratorio = cod_lab;
	UPDATE laboratorios set correo=cor where cod_laboratorio = cod_lab;
	UPDATE laboratorios set direccion=direc where cod_laboratorio = cod_lab;
	UPDATE laboratorios set descripcion=descrip where cod_laboratorio = cod_lab;
END;
$$;


ALTER FUNCTION public.editarlaboratorio(cod_lab integer, nom character varying, tel character varying, cor character varying, direc character varying, descrip character varying) OWNER TO postgres;

--
-- Name: editarproducto(integer, character varying, integer, integer, integer, integer, character varying, date, integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.editarproducto(id_prod integer, nom character varying, id_grup integer, sto integer, precio_u integer, precio_c integer, descrip character varying, fech_ven date, lab integer, codbar character varying, pgananc character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE productos set nombre=nom where id_producto = id_prod;
	UPDATE productos set id_grupo=id_grup where id_producto = id_prod;
	UPDATE productos set stock=sto where id_producto = id_prod;
	UPDATE productos set precio_unidad=precio_u where id_producto = id_prod;
	UPDATE productos set precio_compra=precio_c where id_producto = id_prod;
	UPDATE productos set descripcion=descrip where id_producto = id_prod;
	UPDATE productos set fecha_vencimiento=fech_ven where id_producto = id_prod;
	UPDATE productos set cod_laboratorio=lab where id_producto = id_prod;
	UPDATE productos set codigo_barras=codbar where id_producto = id_prod;
	UPDATE productos set pganancia=pgananc where id_producto = id_prod;
END;
$$;


ALTER FUNCTION public.editarproducto(id_prod integer, nom character varying, id_grup integer, sto integer, precio_u integer, precio_c integer, descrip character varying, fech_ven date, lab integer, codbar character varying, pgananc character varying) OWNER TO postgres;

--
-- Name: editarusuario(character varying, character varying, character varying, character varying, character varying, character varying, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.editarusuario(ced character varying, nom character varying, ape character varying, tel character varying, dir character varying, cor character varying, rol_usuario character varying, pass character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN 
	UPDATE usuarios set nombres=nom where cedula = ced;
	UPDATE usuarios set apellidos=ape where cedula = ced;
	UPDATE usuarios set telefono=tel where cedula = ced;
	UPDATE usuarios set direccion=dir where cedula = ced;
	UPDATE usuarios set correo=cor where cedula = ced;
	UPDATE usuarios set rol=rol_usuario where cedula = ced;
	UPDATE usuarios set password=pass where cedula = ced;
END;
$$;


ALTER FUNCTION public.editarusuario(ced character varying, nom character varying, ape character varying, tel character varying, dir character varying, cor character varying, rol_usuario character varying, pass character varying) OWNER TO postgres;

--
-- Name: nueva_venta(integer, integer, character varying, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.nueva_venta(id_producto_f integer, unidades_f integer, cedula_f character varying, valor_f integer, ganancia_f integer, cod_lab_f integer) RETURNS void
    LANGUAGE plpgsql
    AS $$
	BEGIN 
		UPDATE productos set stock=(productos.stock-unidades_f) where productos.id_producto = id_producto_f;

		insert into ventas(id_producto, unidades, cedula, fecha, valor, ganancia, cod_laboratorio) values
		(id_producto_f,unidades_f,cedula_f,(select localtimestamp),valor_f, ganancia_f, cod_lab_f);
	END;
$$;


ALTER FUNCTION public.nueva_venta(id_producto_f integer, unidades_f integer, cedula_f character varying, valor_f integer, ganancia_f integer, cod_lab_f integer) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: grupos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grupos (
    id_grupo integer NOT NULL,
    nombre character varying,
    descripcion character varying
);


ALTER TABLE public.grupos OWNER TO postgres;

--
-- Name: grupos_id_grupo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grupos_id_grupo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grupos_id_grupo_seq OWNER TO postgres;

--
-- Name: grupos_id_grupo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grupos_id_grupo_seq OWNED BY public.grupos.id_grupo;


--
-- Name: laboratorios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.laboratorios (
    cod_laboratorio integer NOT NULL,
    nombre character varying,
    telefono character varying,
    correo character varying,
    direccion character varying,
    descripcion character varying
);


ALTER TABLE public.laboratorios OWNER TO postgres;

--
-- Name: laboratorios_cod_laboratorio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.laboratorios_cod_laboratorio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.laboratorios_cod_laboratorio_seq OWNER TO postgres;

--
-- Name: laboratorios_cod_laboratorio_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.laboratorios_cod_laboratorio_seq OWNED BY public.laboratorios.cod_laboratorio;


--
-- Name: productos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.productos (
    id_producto integer NOT NULL,
    nombre character varying,
    id_grupo integer,
    stock integer,
    precio_unidad integer,
    precio_compra integer,
    descripcion character varying,
    fecha_vencimiento date,
    cod_laboratorio integer,
    codigo_barras character varying,
    pganancia character varying
);


ALTER TABLE public.productos OWNER TO postgres;

--
-- Name: productos_id_producto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.productos_id_producto_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productos_id_producto_seq OWNER TO postgres;

--
-- Name: productos_id_producto_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.productos_id_producto_seq OWNED BY public.productos.id_producto;


--
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios (
    cedula character varying NOT NULL,
    nombres character varying,
    apellidos character varying,
    telefono character varying,
    direccion character varying,
    correo character varying,
    rol character varying,
    password character varying
);


ALTER TABLE public.usuarios OWNER TO postgres;

--
-- Name: ventas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ventas (
    id_venta integer NOT NULL,
    id_producto integer,
    unidades integer,
    cedula character varying,
    fecha timestamp without time zone,
    valor integer,
    ganancia integer,
    cod_laboratorio integer
);


ALTER TABLE public.ventas OWNER TO postgres;

--
-- Name: ventas_id_venta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ventas_id_venta_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ventas_id_venta_seq OWNER TO postgres;

--
-- Name: ventas_id_venta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ventas_id_venta_seq OWNED BY public.ventas.id_venta;


--
-- Name: grupos id_grupo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupos ALTER COLUMN id_grupo SET DEFAULT nextval('public.grupos_id_grupo_seq'::regclass);


--
-- Name: laboratorios cod_laboratorio; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.laboratorios ALTER COLUMN cod_laboratorio SET DEFAULT nextval('public.laboratorios_cod_laboratorio_seq'::regclass);


--
-- Name: productos id_producto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos ALTER COLUMN id_producto SET DEFAULT nextval('public.productos_id_producto_seq'::regclass);


--
-- Name: ventas id_venta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas ALTER COLUMN id_venta SET DEFAULT nextval('public.ventas_id_venta_seq'::regclass);


--
-- Data for Name: grupos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.grupos VALUES (1, 'Antibiotico', 'Para infecciones');
INSERT INTO public.grupos VALUES (2, 'Psicotrópicos', 'No es de venta libre');
INSERT INTO public.grupos VALUES (3, 'Grupo 5', 'Medicamentos sin receta');


--
-- Data for Name: laboratorios; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.laboratorios VALUES (2, 'Tropipasto', '7202020', '@Correo', '#Direccion', 'Laboratorio importante, medicamentos mas baratos');
INSERT INTO public.laboratorios VALUES (3, 'Distrimayor', '789456123', '@distrimayor.com', '#Direccion distrimayor', 'Comprar todo menos antibioticos...');
INSERT INTO public.laboratorios VALUES (4, 'FARMAPASTO', '7654321', 'farmapasto@123.com', 'Dir 123 # 2-9', '');


--
-- Data for Name: productos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.productos VALUES (3, 'Amoxicilina', 1, 97, 1650, 1500, 'Sin restriccion', '2020-01-31', 3, '54646561654', '10');
INSERT INTO public.productos VALUES (4, 'Diclofenaco', 3, 49, 1150, 1000, 'Con restricciones', '2021-01-31', 2, '88546545744524', '13');
INSERT INTO public.productos VALUES (5, 'Dolex Jarabe', 3, 2, 13200, 12000, 'Sin receta', '2020-02-29', 4, '77754611849', '10');
INSERT INTO public.productos VALUES (6, 'Acetaminofen', 3, 49, 100, 80, 'Sin restriccion, no hay descuentos', '2020-02-29', 2, '88487774454778', '10');


--
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.usuarios VALUES ('1085320429', 'Luis Angel', 'Espana Yepez', '3002384972', 'CRA 4 # 123 Santa Monica', 'correo@gmail.com', 'admin', '1234');


--
-- Data for Name: ventas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.ventas VALUES (1, 3, 2, '1085320429', '2020-01-11 23:14:04.226634', 3300, 300, 3);
INSERT INTO public.ventas VALUES (3, 5, 1, '1085320429', '2020-01-26 17:25:30.628379', 13200, 1200, 4);
INSERT INTO public.ventas VALUES (4, 3, 1, '1085320429', '2020-01-26 17:25:30.633487', 1650, 150, 3);
INSERT INTO public.ventas VALUES (5, 4, 1, '1085320429', '2020-01-28 16:37:48.196412', 1150, 150, 2);
INSERT INTO public.ventas VALUES (6, 5, 2, '1085320429', '2020-01-28 16:37:48.467984', 26400, 2400, 4);
INSERT INTO public.ventas VALUES (7, 6, 1, '1085320429', '2020-01-28 16:37:48.470142', 100, 20, 2);


--
-- Name: grupos_id_grupo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.grupos_id_grupo_seq', 3, true);


--
-- Name: laboratorios_cod_laboratorio_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.laboratorios_cod_laboratorio_seq', 4, true);


--
-- Name: productos_id_producto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.productos_id_producto_seq', 6, true);


--
-- Name: ventas_id_venta_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ventas_id_venta_seq', 7, true);


--
-- Name: grupos grupos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupos
    ADD CONSTRAINT grupos_pkey PRIMARY KEY (id_grupo);


--
-- Name: laboratorios laboratorios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.laboratorios
    ADD CONSTRAINT laboratorios_pkey PRIMARY KEY (cod_laboratorio);


--
-- Name: productos productos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos
    ADD CONSTRAINT productos_pkey PRIMARY KEY (id_producto);


--
-- Name: usuarios usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (cedula);


--
-- Name: ventas ventas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas
    ADD CONSTRAINT ventas_pkey PRIMARY KEY (id_venta);


--
-- Name: productos productos_cod_laboratorio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos
    ADD CONSTRAINT productos_cod_laboratorio_fkey FOREIGN KEY (cod_laboratorio) REFERENCES public.laboratorios(cod_laboratorio);


--
-- Name: productos productos_id_grupo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.productos
    ADD CONSTRAINT productos_id_grupo_fkey FOREIGN KEY (id_grupo) REFERENCES public.grupos(id_grupo);


--
-- Name: ventas ventas_cedula_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas
    ADD CONSTRAINT ventas_cedula_fkey FOREIGN KEY (cedula) REFERENCES public.usuarios(cedula);


--
-- Name: ventas ventas_cod_laboratorio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas
    ADD CONSTRAINT ventas_cod_laboratorio_fkey FOREIGN KEY (cod_laboratorio) REFERENCES public.laboratorios(cod_laboratorio);


--
-- Name: ventas ventas_id_producto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ventas
    ADD CONSTRAINT ventas_id_producto_fkey FOREIGN KEY (id_producto) REFERENCES public.productos(id_producto);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--